<?php

/**
 * LoginModel
 *
 * Handles the user's login / logout / registration stuff
 */

class LoginModel extends Model
{

	public function login($username, $password){
        require LIBS_PATH . "password.php";
		// get user's data
        // (we check if the password fits the password_hash via password_verify() some lines below)
        $sth = $this->db->prepare("SELECT * FROM users WHERE username = :username OR email = :username");
        // DEFAULT is the marker for "normal" accounts (that have a password etc.)
        // There are other types of accounts that don't have passwords etc. (FACEBOOK)
        $sth->execute(array(':username' => $username));
        $count =  $sth->rowCount();
        // if there's NOT one result
        if ($count != 1) {
            // was FEEDBACK_USER_DOES_NOT_EXIST before, but has changed to FEEDBACK_LOGIN_FAILED
            // to prevent potential attackers showing if the user exists
            return false;
        }

        // fetch one row (we only have one result)
        $result = $sth->fetch();

        // block login attempt if somebody has already failed 3 times and the last login attempt is less than 30sec ago
        if (($result->failed_logins >= 3) AND ($result->last_failed_login > (time()-30))) {
            echo "ignored";
            return false;
        }

        // check if hash of provided password matches the hash in the database
        if (password_verify($password, $result->password)) {

            if ($result->banned) {
                echo "user is banned";
                return false;
            }

            // login process, write the user data into session
            Session::init();
            Session::set('user_logged_in', true);
            Session::set('user_id', $result->ID);
            Session::set('username', $result->username);
            Session::set('active_list', $result->active_list);
            Session::set('email', $result->email);

            // reset the failed login counter for that user (if necessary)
            if ($result->last_failed_login > 0) {
                $sql = "UPDATE users SET failed_logins = 0, last_failed_login = NULL
                        WHERE ID = :user_id AND failed_logins != 0";
                $sth = $this->db->prepare($sql);
                $sth->execute(array(':user_id' => $result->ID));
            }

            // generate integer-timestamp for saving of last-login date
            $user_last_login_timestamp = time();
            // write timestamp of this login into database (we only write "real" logins via login form into the
            // database, not the session-login on every page request
            $sql = "UPDATE users SET last_login = NOW() WHERE ID = :user_id";
            $sth = $this->db->prepare($sql);
            $sth->execute(array(':user_id' => $result->ID));

            // if user has checked the "remember me" checkbox, then write cookie
            if (isset($_POST['rememberme'])) {

                // generate 64 char random string
                $random_token_string = hash('sha256', mt_rand());

                // write that token into database
                $sql = "UPDATE users SET rememberme_token = :user_rememberme_token WHERE ID = :user_id";
                $sth = $this->db->prepare($sql);
                $sth->execute(array(':user_rememberme_token' => $random_token_string, ':user_id' => $result->ID));

                // generate cookie string that consists of user id, random string and combined hash of both
                $cookie_string_first_part = $result->ID . ':' . $random_token_string;
                $cookie_string_hash = hash('sha256', $cookie_string_first_part);
                $cookie_string = $cookie_string_first_part . ':' . $cookie_string_hash;

                // set cookie
                setcookie('rememberme', $cookie_string, time() + COOKIE_RUNTIME, "/");

                //print_r($_COOKIE);
            }

            // return true to make clear the login was successful
            return true;

        } else {

            // increment the failed login counter for that user
            $sql = "UPDATE users
                    SET failed_logins = failed_logins+1, last_failed_login = :user_last_failed_login
                    WHERE username = :user_name OR email = :user_name";
            $sth = $this->db->prepare($sql);
            $sth->execute(array(':user_name' => $username, ':user_last_failed_login' => time()));
            
            return false;
        }

        // default return
        return false;
	}

    /**
     * performs the login via cookie
     * @return bool success state
     */
    public function loginWithCookie()
    {
        $cookie = isset($_COOKIE['rememberme']) ? $_COOKIE['rememberme'] : '';

        // do we have a cookie var ?
        if (!$cookie) {
            echo "there is no cookie";
            return false;
        }

        // check cookie's contents, check if cookie contents belong together
        list($user_id, $token, $hash) = explode(':', $cookie);
        if ($hash !== hash('sha256', $user_id . ':' . $token)) {
            return false;
        }

        // do not log in when token is empty
        if (empty($token)) {
            return false;
        }

        // get real token from database (and all other data)
        $query = $this->db->prepare("SELECT *
                                     FROM users
                                     WHERE ID = :user_id
                                       AND rememberme_token = :user_rememberme_token
                                       AND rememberme_token IS NOT NULL");
        $query->execute(array(':user_id' => $user_id, ':user_rememberme_token' => $token));
        $count = $query->rowCount();
        if ($count == 1) {
            // fetch one row (we only have one result)
            $result = $query->fetch();
            // TODO: this block is same/similar to the one from login(), maybe we should put this in a method
            // write data into session
            Session::init();
            Session::set('user_logged_in', true);
            Session::set('user_id', $result->ID);
            Session::set('username', $result->username);
            Session::set('email', $result->email);

            // generate integer-timestamp for saving of last-login date
            $user_last_login_timestamp = time();
            // write timestamp of this login into database (we only write "real" logins via login form into the
            // database, not the session-login on every page request
            $sql = "UPDATE users SET last_login = :user_last_login_timestamp WHERE ID = :user_id";
            $sth = $this->db->prepare($sql);
            $sth->execute(array(':user_id' => $user_id, ':user_last_login_timestamp' => $user_last_login_timestamp));

            return true;
        } else {
            return false;
        }
    }

	/**
     * Returns the current state of the user's login
     * @return bool user's login status
     */
    public function isUserLoggedIn()
    {
        return Session::get('user_logged_in');
    }

    /**
     * Log out process, deletes cookie, deletes session
     */
    public function logout()
    {
        // set the remember-me-cookie to ten years ago (3600sec * 365 days * 10).
        // that's obviously the best practice to kill a cookie via php
        // @see http://stackoverflow.com/a/686166/1114320
        //setcookie('rememberme', false, time() - (3600 * 3650), '/', COOKIE_DOMAIN);

        // delete the session
        Session::destroy();
    }

    /**
     * Deletes the (invalid) remember-cookie to prevent infinitive login loops
     */
    public function deleteCookie()
    {
        // set the rememberme-cookie to ten years ago (3600sec * 365 days * 10).
        // that's obviously the best practice to kill a cookie via php
        // @see http://stackoverflow.com/a/686166/1114320
        setcookie('rememberme', false, time() - (3600 * 3650), '/');
    }

    /**
     * handles the entire registration process for DEFAULT users (not for people who register with
     * 3rd party services, like facebook) and creates a new user in the database if everything is fine
     * @return boolean Gives back the success status of the registration
     */
    public function registerNewUser($username, $email, $password)
    {
        require LIBS_PATH . "password.php";
        // clean the input
        $username = strip_tags($username);
        $email = strip_tags($email);

        // crypt the user's password with the PHP 5.5's password_hash() function, results in a 60 character
        // hash string. the PASSWORD_DEFAULT constant is defined by the PHP 5.5, or if you are using PHP 5.3/5.4,
        // by the password hashing compatibility library. the third parameter looks a little bit shitty, but that's
        // how those PHP 5.5 functions want the parameter: as an array with, currently only used with 'cost' => XX
        $user_password_hash = password_hash($password, PASSWORD_DEFAULT, array('cost' => 10));

        // check if username already exists
        $query = $this->db->prepare("SELECT ID FROM users WHERE username = :user_name");
        $query->execute(array(':user_name' => $username));
        $count = $query->rowCount();
        if ($count == 1) {
            Register::$error['username'] = "This username already exists.";
            return false;
        }

        // check if email already exists
        $query = $this->db->prepare("SELECT ID FROM users WHERE email = :user_email");
        $query->execute(array(':user_email' => $email));
        $count = $query->rowCount();
        if ($count >= 1) {
            Register::$error['email'] = "This email address already exists.";
            return false;
        }

        // write new users data into database
        $sql = "INSERT INTO users (username, password, email)
                VALUES (:user_name, :user_password_hash, :user_email)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':user_name' => $username,
                              ':user_password_hash' => $user_password_hash,
                              ':user_email' => $email));
        $count = $query->rowCount();
        if ($count != 1) {
            Register::$error['username'] = "Failed to create your account for some reason.";
            return false;
        }else{
            return true;
        }
        // default return, returns only true of really successful (see above)
        return false;
    }

}
?>