<?php

class CommentModel extends Model
{


    /**
     * Get total number of comments from the database
     */
    public function getTotalComments()
    {
        $sql = "SELECT COUNT(ID) AS total FROM comments";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetch()->total;
    }

    
    /**
     * Report comment
     */
    public function report($comment_id, $user_id)
    {
        if($this->hasBeenReported($comment_id, $user_id))
            $sql = "DELETE FROM comment_reports WHERE comment_id = :comment_id AND user_id = :user_id";
        else
            $sql = "INSERT INTO comment_reports (comment_id, user_id) VALUES (:comment_id, :user_id)";

        $query = $this->db->prepare($sql);
        $query->execute(array(':comment_id' => $comment_id, ':user_id' => $user_id));

        return true;
    }

    /**
     * Determine if a comment has been reported by a user
     */
    public function hasBeenReported($comment_id, $user_id)
    {

        $query = $this->db->prepare("SELECT * FROM comment_reports WHERE comment_id = :comment_id AND user_id = :user_id");
        $query->execute(array(':comment_id' => $comment_id, ':user_id' => $user_id));

        return $query->rowCount();
    }

    /**
     * Get comments for an item
     */
    public function getItemComments($item_id, $parent=0)
    {
        $sql = "SELECT comments.*, users.username FROM comments INNER JOIN users ON comments.user_id = users.ID WHERE submission_id = :item_id AND parent = :parent ORDER BY date_posted DESC";
        $query = $this->db->prepare($sql);
        $query->execute(array(':item_id' => $item_id, ':parent' => $parent));
        return $query->fetchAll();
    }

    /**
     * Get specific comment
     */
    public function getComment($comment_id)
    {
        $sql = "SELECT comments.*, users.username, users.ID AS user_id FROM comments INNER JOIN users ON comments.user_id = users.ID WHERE comments.ID = :comment_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':comment_id' => $comment_id));
        return $query->fetch();
    }

    /**
     * Get author of comment
     */
    public function getAuthor($comment_id)
    {
        $sql = "SELECT user_id FROM comments WHERE ID = :comment_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':comment_id' => $comment_id));
        return $query->fetch()->user_id;
    }

    /**
     * Get number of children comment has
     */
    public function getNumChildren($comment_id)
    {
        $sql = "SELECT * FROM comments WHERE parent = :comment_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':comment_id' => $comment_id));
        return $query->rowCount();
    }

    /**
     * Add a comment to an item
     */
    public function postComment($item_id, $user_id, $content, $parent)
    {        
        if($item_id == "undefined" && $parent > 0)
            $item_id = $this->getSingleValue("SELECT submission_id FROM comments WHERE ID = $parent");

        $sql = "INSERT INTO comments (user_id, submission_id, date_posted, date_edited, parent, content) VALUES (:user_id, :submission_id, NOW(), NULL, :parent, :content)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':user_id' => $user_id, ':submission_id' => $item_id, ':parent' => $parent, ':content' => $content));

        return $this->getComment($this->db->lastInsertId());
    }


    /**
     * Edits a comment
     */
    public function editComment($comment_id, $source)
    {
        $query = $this->db->prepare("UPDATE comments SET content = :content, date_edited = NOW() WHERE ID = :comment_id");
        $query->execute(array(':comment_id' => $comment_id, ':content' => $source));

        return $query->rowCount();
    }

    /**
     * Get reported comments
     */
    public function getReportedComments()
    {
        $sql = "SELECT comments.*, users.username FROM comments INNER JOIN users ON comments.user_id = users.ID WHERE (SELECT COUNT(*) FROM comment_reports WHERE comment_id = comments.ID) > 0";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Remove all reports for comment
     */
    public function clearReports($comment_id)
    {

        $query = $this->db->prepare("DELETE FROM comment_reports WHERE comment_id = :comment_id");
        $query->execute(array(':comment_id' => $comment_id));

        return $query->rowCount();
    }

    /**
     * Remove comment from site
     */
    public function removeComment($comment_id)
    {

        $query = $this->db->prepare("UPDATE comments SET removed = 1 WHERE ID = :comment_id");
        $query->execute(array(':comment_id' => $comment_id));

        return $query->rowCount();
    }

    


    
}
