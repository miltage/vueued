<?php

class UserModel extends Model
{


    /**
     * Get a category ID from its name
     */
    public function getUserID($username)
    {
        return $this->getSingleValue("SELECT ID FROM users WHERE username = '$username'");
    }

    /**
     * Get total number of registered users on the system
     */
    public function getTotalUsers()
    {
        $sql = "SELECT COUNT(ID) AS total FROM users";
        $query = $this->db->prepare($sql);
        $query->execute();
        
        return $query->fetch()->total;
    }

    /**
     * Get last 5 active users
     */
    public function getLastActive()
    {
        $sql = "SELECT username, last_activity FROM users WHERE ID != :user_id ORDER BY last_activity DESC LIMIT 5";
        $query = $this->db->prepare($sql);
        $query->execute(array(':user_id' => Session::get("user_id")));
        
        return $query->fetchAll();
    }

    /**
     * Get user information
     */
    public function getUserInfo($user_id)
    {
        $sql = "SELECT * FROM users WHERE ID = :user_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':user_id' => $user_id));
        
        return $query->fetch();
    }

    /**
     * Determine if user is an admin
     */
    public function isAdmin($user_id)
    {
        $query = $this->db->prepare("SELECT * FROM admins WHERE user_id = :user_id");
        $query->execute(array(':user_id' => $user_id));

        return $query->rowCount();
    }

    /**
     * Determine if user is banned
     */
    public function isBanned($user_id)
    {
        $query = $this->db->prepare("SELECT banned FROM users WHERE ID = :user_id");
        $query->execute(array(':user_id' => $user_id));

        return $query->fetch()->banned;
    }

    /**
     * Update social media link on profile
     */
    public function setProfileLink($user_id, $type, $link)
    {
        if (!empty($link) && preg_match("#https?://#", $link) === 0) {
            $link = 'http://'.$link;
        }

        $query = $this->db->prepare("UPDATE users SET `$type` = :link WHERE ID = :user_id");
        $query->execute(array(':user_id' => $user_id, ':link' => $link));

        return $query->rowCount();
    }

    


    
}
