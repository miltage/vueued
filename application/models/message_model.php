<?php

class MessageModel extends Model
{


    /**
     * Get the ID for a thread using the thread's unique identifier
     */
    public function getThreadID($unique_id)
    {
        return $this->getSingleValue("SELECT ID FROM message_threads WHERE unique_id = '$unique_id'");
    }

    /**
     * Get user information
     */
    public function getUserMessages($user_id)
    {
        $sql = "SELECT *, (SELECT date FROM messages WHERE thread_id = message_threads.ID ORDER BY date DESC LIMIT 1) AS last_activity FROM message_threads WHERE (SELECT COUNT(*) FROM message_users WHERE thread_id = ID AND user_id = :user_id) ORDER BY last_activity DESC";
        $query = $this->db->prepare($sql);
        $query->execute(array(':user_id' => $user_id));
        
        return $query->fetchAll();
    }

    /**
     * Get user information
     */
    public function getThread($thread_id)
    {
        $sql = "SELECT * FROM message_threads WHERE ID = :thread_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':thread_id' => $thread_id));
        
        return $query->fetch();
    }

    /**
     * Get user information
     */
    public function getThreadMessages($thread_id)
    {
        $sql = "SELECT * FROM messages INNER JOIN users ON users.ID = messages.user_id WHERE thread_id = :thread_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':thread_id' => $thread_id));
        
        return $query->fetchAll();
    }

    /**
     * Get thread events
     */
    public function getThreadEvents($thread_id)
    {
        $sql = "SELECT * FROM message_events WHERE thread_id = :thread_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':thread_id' => $thread_id));
        
        return $query->fetchAll();
    }

    /**
     * Gets all users that have access to a thread
     */
    public function getThreadUsers($thread_id)
    {
        $sql = "SELECT * FROM message_users INNER JOIN users ON users.ID = user_id WHERE thread_id = :thread_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':thread_id' => $thread_id));
        
        return $query->fetchAll();
    }

    /**
     * Get time since user last read a thread
     */
    public function getLastRead($thread_id, $user_id)
    {
        $sql = "SELECT last_read FROM message_users WHERE thread_id = :thread_id AND user_id = :user_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':thread_id' => $thread_id, ':user_id' => $user_id));
        
        return $query->fetch()->last_read;
    }

    /**
     * Update the time since user last read a thread
     */
    public function setLastRead($thread_id, $user_id)
    {
        $sql = "UPDATE message_users SET last_read = NOW() WHERE thread_id = :thread_id AND user_id = :user_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':thread_id' => $thread_id, ':user_id' => $user_id));
        
        return 1;
    }

    /**
     * Determine if user has permission to read thread
     */
    public function canRead($thread_id, $user_id)
    {
        $query = $this->db->prepare("SELECT * FROM message_users WHERE thread_id = :thread_id AND user_id = :user_id");
        $query->execute(array(':thread_id' => $thread_id, ':user_id' => $user_id));

        return $query->rowCount();
    }

    /**
     * Determine if user is owner of a particular thread
     */
    public function isOwner($thread_id, $user_id)
    {
        $query = $this->db->prepare("SELECT owner FROM message_threads WHERE ID = :thread_id");
        $query->execute(array(':thread_id' => $thread_id));

        return $query->fetch()->owner == $user_id;
    }

    /**
     * Determine if user has unread messages
     */
    public function hasUnreadMessages($user_id)
    {
        $messages = $this->getUserMessages($user_id);

        foreach($messages as $message){
            $last_read = $this->getLastRead($message->ID, $user_id);
            $unread = TimeUtil::timeCompare($message->last_activity, $last_read);
            if($unread) return true;
        }

        return false;
    }


    /**
     * Post message to thread
     */
    public function sendMessage($thread_id, $user_id, $content)
    {
        if(!isset($thread_id) || !isset($user_id) || !isset($content))
            return json_encode(array("success" => false, "result" => "Fatal error: variables missing."));

        $sql = "INSERT INTO messages (thread_id, user_id, date, content) VALUES (:thread_id, :user_id, NOW(), :content)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':thread_id' => $thread_id, ':user_id' => $user_id, ':content' => $content));

        return json_encode(array("success" => true, "result" => 1));
    }

    /**
     * Manage thread users, adding and deleting where necessary
     */
    public function editThreadUsers($thread_id, $recipients)
    {
        foreach($recipients as $recipient){
            $user_id = $this->getSingleValue("SELECT ID FROM users WHERE username = '$recipient'");
            if(!$user_id){
                Message::$error['recipients'] = "One or more of these users do not exist.";
                return false;
            }
        }

        $added = array();
        $removed = array();

        // Attach recipients to thread if they aren't already
        foreach($recipients as $recipient){
            $user_id = $this->getSingleValue("SELECT ID FROM users WHERE username = '$recipient'");
            if(!$this->canRead($thread_id, $user_id))
                $added[] = $recipient;
            $sql = "INSERT INTO message_users (thread_id, user_id, last_read) 
                        SELECT :thread_id, :user_id, '0000-00-00 00:00:00' FROM DUAL
                            WHERE NOT EXISTS (SELECT * FROM message_users 
                                WHERE thread_id=:thread_id AND user_id=:user_id) LIMIT 1";
            $query = $this->db->prepare($sql);
            $query->execute(array(':thread_id' => $thread_id, ':user_id' => $user_id));
        }

        // Remove recipients from thread if they aren't in the submitted list
        $users = $this->getThreadUsers($thread_id);
        $search_array = array_map('strtolower', $recipients);
        foreach ($users as $user) {
            if(!in_array(strtolower($user->username), $search_array) && !$this->isOwner($thread_id, $user->ID)){
                $removed[] = $user->username;
                $sql = "DELETE FROM message_users WHERE thread_id = :thread_id AND user_id = :user_id";
                $query = $this->db->prepare($sql);
                $query->execute(array(':thread_id' => $thread_id, ':user_id' => $user->ID));
            }
        }

        if(count($added)){
            $event = Session::get("username")." added ".Util::formatList($added)." to the thread.";
            $this->createThreadEvent($thread_id, $event);
        }

        if(count($removed)){
            $event = Session::get("username")." removed ".Util::formatList($removed)." from the thread.";
            $this->createThreadEvent($thread_id, $event);
        }

        return json_encode(array("success" => true, "result" => 1));
    }

    /**
     * Add user to thread
     */
    public function addUserToThread($thread_id, $user_id)
    {
        if(!isset($thread_id) || !isset($user_id))
            return json_encode(array("success" => false, "result" => "Fatal error: variables missing."));

        $sql = "INSERT INTO messages_users (thread_id, user_id, date) VALUES (:thread_id, :user_id, '0000-00-00 00:00:00')";
        $query = $this->db->prepare($sql);
        $query->execute(array(':thread_id' => $thread_id, ':user_id' => $user_id));

        return json_encode(array("success" => true, "result" => 1));
    }

    /**
     * Remove user from thread
     */
    public function removeUserFromThread($thread_id, $user_id)
    {
        $sql = "DELETE FROM message_users WHERE thread_id = :thread_id AND user_id = :user_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':thread_id' => $thread_id, ':user_id' => $user_id));

        return json_encode(array("success" => true, "result" => 1));
    }

    /**
     * Create event for thread
     */
    public function createThreadEvent($thread_id, $content)
    {
        $sql = "INSERT INTO message_events (thread_id, content, date) VALUES (:thread_id, :content, NOW())";
        $query = $this->db->prepare($sql);
        return $query->execute(array(':thread_id' => $thread_id, ':content' => $content));
    }

    /**
    *   Composes a new message
    */
    public function composeMessage($recipients, $subject, $content, $owner_id){

        foreach($recipients as $recipient){
            $user_id = $this->getSingleValue("SELECT ID FROM users WHERE username = '$recipient'");
            if(!$user_id){
                Inbox::$error['recipients'] = "One or more of these users do not exist.";
                return false;
            }
        }

        $unique_id = $this->generate_unique_id(6);
        while($this->getSingleValue("SELECT unique_id FROM message_threads WHERE unique_id = '$unique_id'"))
            $unique_id = $this->generate_unique_id(6);

        // Create message thread
        $sql = "INSERT INTO message_threads (unique_id, owner, date, subject) VALUES (:unique_id, :owner, NOW(), :subject)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':unique_id' => $unique_id, ':owner' => $owner_id, ':subject' => $subject));

        $thread_id = $this->db->lastInsertId();

        // Attach owner to thread
        $sql = "INSERT INTO message_users (thread_id, user_id) VALUES (:thread_id, :user_id)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':thread_id' => $thread_id, ':user_id' => $owner_id));

        // Attach recipients to thread
        foreach($recipients as $recipient){
            $user_id = $this->getSingleValue("SELECT ID FROM users WHERE username = '$recipient'");
            $sql = "INSERT INTO message_users (thread_id, user_id, last_read) 
                        SELECT :thread_id, :user_id, '0000-00-00 00:00:00' FROM DUAL
                            WHERE NOT EXISTS (SELECT * FROM message_users 
                                WHERE thread_id=:thread_id AND user_id=:user_id) LIMIT 1";
            $query = $this->db->prepare($sql);
            $query->execute(array(':thread_id' => $thread_id, ':user_id' => $user_id));
        }

        // Insert first message from owner
        $this->sendMessage($thread_id, $owner_id, $content);

        return true;
    }

    private function generate_unique_id($length) {
        $random = '';
        for ($i = 0; $i < $length; $i++) {
            $random .= rand(0, 1) ? rand(0, 9) : chr(rand(ord('a'), ord('z')));
        }
        return $random;
    }

    


    
}
