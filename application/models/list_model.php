<?php

class ListModel extends Model
{

    /**
     * Get all lists for a user
     */
    public function getUserLists($user_id)
    {
        $sql = "SELECT * FROM lists WHERE user_id = :user_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':user_id' => $user_id));
        
        return $query->fetchAll();
    }

    /**
     * Get information for a specific list
     */
    public function getList($list_id)
    {
        $sql = "SELECT * FROM lists WHERE ID = :list_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':list_id' => $list_id));
        
        return $query->fetch();
    }
    
    /**
     * Get all items for a list
     */
    public function getListItems($list_id)
    {
        $sql = "SELECT DISTINCT * FROM list_items INNER JOIN tags ON ID = tag_id WHERE list_id = :list_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':list_id' => $list_id));

        return $query->fetchAll();
    }

    /**
     * Add an item to a list
     *  @todo: make sure logged in user is owner of list before deleting, otherwise anyone can add items by entering random numbers
     */
    public function addItemToList($list_id, $item_id)
    {
        if(!$item_id) return json_encode(array("success" => false, "result" => "That tag does not yet exist!"));
        
        // Make sure item isn't already on list
        $sql = "SELECT COUNT(*) AS amount FROM list_items WHERE list_id = :list_id AND tag_id = :tag_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':list_id' => $list_id, ':tag_id' => $item_id));
        $num = $query->fetch()->amount;

        if($num) return json_encode(array("success" => false, "result" => "That item is already on the list!"));

        $sql = "INSERT INTO list_items (list_id, tag_id) VALUES (:list_id, :tag_id)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':list_id' => $list_id, ':tag_id' => $item_id));

        $name = $this->getSingleValue("SELECT title FROM tags WHERE ID = $item_id");
        return json_encode(array("success" => true, "result" => $name));
    }

    /**
     * Remove an item to a list
     */
    public function removeItemFromList($list_id, $item_id)
    {

        // TODO: make sure logged in user is owner of list before deleting, otherwise anyone can delete items by entering random numbers

        $sql = "DELETE FROM list_items WHERE list_id = :list_id AND tag_id = :tag_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':list_id' => $list_id, ':tag_id' => $item_id));

        return json_encode(array("success" => true));
    }

    /**
     * Rename a list
     */
    public function renameList($list_id, $list_title)
    {

        // TODO: make sure logged in user is owner of list before deleting, otherwise anyone can delete items by entering random numbers

        $sql = "UPDATE lists SET title = :list_title WHERE ID = :list_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':list_id' => $list_id, ':list_title' => $list_title));

        return json_encode(array("success" => true));
    }

    /**
     * Delete a list
     */
    public function deleteList($list_id)
    {

        // TODO: make sure logged in user is owner of list before deleting, otherwise anyone can delete items by entering random numbers

        $sql = "DELETE FROM list_items WHERE list_id = :list_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':list_id' => $list_id));

        $sql = "DELETE FROM lists WHERE ID = :list_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':list_id' => $list_id));

        return json_encode(array("success" => true));
    }

    /**
     * Create a new list
     */
    public function addList()
    {

        $sql = "INSERT INTO lists (title, user_id) VALUES ('Untitled List', :user_id)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':user_id' => Session::get("user_id")));

        return $this->db->lastInsertId();
    }


}
