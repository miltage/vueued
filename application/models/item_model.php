<?php

class ItemModel extends Model
{

    public $fresh = "date + (SELECT COUNT(*) FROM views WHERE submission_id = ID)*100000";
    public $nsfw_filter = "(SELECT COUNT(*) FROM tag_listing INNER JOIN tags ON tag_listing.tag_id = tags.ID WHERE item_id = submissions.ID AND tags.title = 'nsfw') = 0";

    public $default_lists = array(
        /* default */           array("images", "image", "picture", "funny", "videos", "vine", "art"),
        /* default_images */    array("images", "image", "picture"),
        /* default_videos */    array("videos", "video", "vine"),
        /* default_music */     array("music", "music videos"),
        /* default_movies */    array("movies", "trailers")
    );


    /**
     * Get all items from database
     */
    public function getItems()
    {
        $sql = "SELECT * FROM submissions";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Get total number of items from the database
     */
    public function getTotalItems()
    {
        $sql = "SELECT COUNT(ID) AS total FROM submissions";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetch()->total;
    }

    /**
     * Get items from database that have been reported
     */
    public function getReportedItems()
    {
        $sql = "SELECT * FROM submissions WHERE (SELECT COUNT(*) FROM reports WHERE submission_id = submissions.ID) > 0";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Get item from its url
     */
    public function getItemByURL($url)
    {
        $sql = "SELECT * FROM submissions WHERE url = :url OR original_url = :url OR source = :url";
        $query = $this->db->prepare($sql);
        $query->execute(array(':url' => $url));

        return $query->fetchAll();
    }

    /**
     * Get all items from database based on freshness formula
     */
    public function getFresh()
    {
        $sql = "SELECT *, $this->fresh AS score FROM submissions WHERE visible = 1 AND $this->nsfw_filter ORDER BY score DESC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Get all items from database based on popularity
     */
    public function getBest()
    {
        $sql = "SELECT * FROM submissions WHERE visible = 1 AND $this->nsfw_filter ORDER BY views DESC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Get items from the database in chronological order
     */
    public function getLatestItems()
    {
        $sql = "SELECT * FROM submissions WHERE visible = 1 AND $this->nsfw_filter ORDER BY date DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Get items from the database for a paticular category based on freshness formula
     */
    public function getTagItems($tag_id)
    {
        $sql = "SELECT *, $this->fresh AS score FROM submissions WHERE visible = 1 AND (SELECT COUNT(*) FROM tag_listing WHERE item_id = submissions.ID AND tag_id = :tag_id) ORDER BY score DESC";
        $query = $this->db->prepare($sql);
        $query->execute(array(':tag_id' => $tag_id));
        return $query->fetchAll();
    }

    /**
     * Get items from the database for a paticular category ordered chronologically
     */
    public function getLatestTagItems($tag_id)
    {
        $sql = "SELECT * FROM submissions WHERE visible = 1 AND (SELECT COUNT(*) FROM tag_listing WHERE item_id = submissions.ID AND tag_id = :tag_id)  ORDER BY date DESC";
        $query = $this->db->prepare($sql);
        $query->execute(array(':tag_id' => $tag_id));
        return $query->fetchAll();
    }

    /**
     * Get items from the database for a paticular category ordered chronologically
     */
    public function getGreatestTagItems($tag_id)
    {
        $sql = "SELECT * FROM submissions WHERE visible = 1 AND (SELECT COUNT(*) FROM tag_listing WHERE item_id = submissions.ID AND tag_id = :tag_id)  ORDER BY views DESC";
        $query = $this->db->prepare($sql);
        $query->execute(array(':tag_id' => $tag_id));
        return $query->fetchAll();
    }

    /**
     * Get items from the database that belong to a particular user
     */
    public function getUserItems($user_id)
    {
        $sql = "SELECT * FROM submissions WHERE visible = 1 AND user_id = :user_id ORDER BY date DESC";
        $query = $this->db->prepare($sql);
        $query->execute(array(':user_id' => $user_id));
        return $query->fetchAll();
    }

    /**
     * Get all items in a specific list from database based on freshness formula
     */
    public function getFreshFromList($list_id, $list_model)
    {
        $list_items = $list_model->getListItems($list_id);

        $where = "visible = 1 AND (SELECT COUNT(*) FROM tag_listing WHERE item_id = submissions.ID AND (";
        foreach($list_items as $list_item)
            $where .= "tag_id = ".$list_item->ID." || ";
        $where = substr($where, 0, count($where)-4)."))";

        if(!count($list_items)) $where = "visible = 1";

        $sql = "SELECT *, $this->fresh AS score FROM submissions WHERE $where ORDER BY score DESC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Get all items in a specific list from database based on popularity
     */
    public function getBestFromList($list_id, $list_model)
    {
        $list_items = $list_model->getListItems($list_id);

        $where = "visible = 1 AND (SELECT COUNT(*) FROM tag_listing WHERE item_id = submissions.ID AND (";
        foreach($list_items as $list_item)
            $where .= "tag_id = ".$list_item->ID." || ";
        $where = substr($where, 0, count($where)-4)."))";

        if(!count($list_items)) $where = "visible = 1";

        $sql = "SELECT * FROM submissions WHERE $where ORDER BY views DESC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Get items in a specific list from the database in chronological order
     */
    public function getLatestItemsFromList($list_id, $list_model)
    {
        $list_items = $list_model->getListItems($list_id);

        $where = "visible = 1 AND (SELECT COUNT(*) FROM tag_listing WHERE item_id = submissions.ID AND (";
        foreach($list_items as $list_item)
            $where .= "tag_id = ".$list_item->ID." || ";
        $where = substr($where, 0, count($where)-4)."))";

        if(!count($list_items)) $where = "visible = 1";

        $sql = "SELECT * FROM submissions WHERE $where ORDER BY date DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Get all items in a specific list from database based on freshness formula
     */
    public function getFreshFromDefault($list_id)
    {
        $default_list = $this->default_lists[abs($list_id)];

        $where = "visible = 1 AND (SELECT COUNT(*) FROM tag_listing INNER JOIN tags ON tag_listing.tag_id = tags.ID WHERE item_id = submissions.ID AND (";
        foreach($default_list as $list_item)
            $where .= "tags.title = '".$list_item."' || ";
        $where = substr($where, 0, count($where)-4)."))";

        if(!count($default_list)) $where = "visible = 1";

        $sql = "SELECT *, $this->fresh AS score FROM submissions WHERE $where ORDER BY score DESC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Get all items in a specific list from database based on popularity
     */
    public function getBestFromDefault($list_id)
    {
        $default_list = $this->default_lists[abs($list_id)];

        $where = "visible = 1 AND (SELECT COUNT(*) FROM tag_listing INNER JOIN tags ON tag_listing.tag_id = tags.ID WHERE item_id = submissions.ID AND (";
        foreach($default_list as $list_item)
            $where .= "tags.title = '".$list_item."' || ";
        $where = substr($where, 0, count($where)-4)."))";

        if(!count($default_list)) $where = "visible = 1";

        $sql = "SELECT * FROM submissions WHERE $where ORDER BY views DESC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Get items in a specific list from the database in chronological order
     */
    public function getLatestItemsFromDefault($list_id)
    {
        $default_list = $this->default_lists[abs($list_id)];

        $where = "visible = 1 AND (SELECT COUNT(*) FROM tag_listing INNER JOIN tags ON tag_listing.tag_id = tags.ID WHERE item_id = submissions.ID AND (";
        foreach($default_list as $list_item)
            $where .= "tags.title = '".$list_item."' || ";
        $where = substr($where, 0, count($where)-4)."))";

        if(!count($default_list)) $where = "visible = 1";

        $sql = "SELECT * FROM submissions WHERE $where ORDER BY date DESC";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
    
    /**
     * Load item from database
     */
    public function loadItem($item_id) 
    {
        // This is old, but I kept it around for interest's sake
        // Increase the item views
        $sql = "UPDATE submissions SET views = views+1 WHERE ID = $item_id";
        $this->db->query($sql);

        // If the user is logged in, add view if one doesn't already exist
        if(Session::isLoggedIn()){
            $sql = "INSERT INTO views (submission_id, user_id) 
                    SELECT :item_id, :user_id FROM DUAL 
                    WHERE NOT EXISTS (SELECT * FROM views 
                          WHERE submission_id = :item_id AND user_id = :user_id) LIMIT 1";
            $query = $this->db->prepare($sql);
            $query->execute(array(':item_id' => $item_id, ':user_id' => Session::get("user_id")));
        }

        // Get item from database
        $sql = "SELECT * FROM submissions 
        INNER JOIN users ON users.ID = submissions.user_id
        WHERE submissions.ID = $item_id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll()[0];
    }

    /**
     * Insert item into database
     */
    public function insertItem($title, $url, $tags, $source) 
    {
        ob_start();
        $title = strip_tags($title);
        $url = strip_tags($url);
        $tags = explode(",", $tags);
        $source = strip_tags($source);
        $time = time();

        // turn on for debugging
        ini_set("display_errors", 1);

        // Get url info
        $url_info = parse_url($url);
        $host = Util::parseDomain($url);
        $path = pathinfo($url_info['path']);
        $ext = isset($path['extension'])?$path['extension']:"";


        if($ext == "jpg" || $ext == "png" || $ext == "gif"){
            $content_type = IMAGE;
        }
        else if($host == "imgur.com"){
            $content_type = IMAGE;

            if(empty($source) && stripos($url, "/a/")) $source = $url;
            $original_url = $url;

            // Imgur album or hosted image posted - get direct image url
            $imgur = file_get_contents($url);
            preg_match('/rel="image_src"\s*href="(.*?)"/', $imgur, $matches);
            if($matches[1]) $url = $matches[1];
            else {
                Submit::$error['general'] = "Could not retrieve imgur image source.";
                return false;
            }

            $url_info = parse_url($url);
            $host = Util::parseDomain($url);
            $path = pathinfo($url_info['path']);
            $ext = isset($path['extension'])?$path['extension']:"";
        }
        else if($host == "youtube.com"){
            $content_type = YOUTUBE;
        }
        else if($host == "vine.co"){
            $content_type = VINE;
        }
        else if($host == "vimeo.com"){
            $content_type = VIMEO;
        }
        else if($host == "deviantart.com"){
            $content_type = DEVIANTART;
        }
        else if($ext == "gifv" || $ext == "webm" || $ext == "mp4"){
            $content_type = VIDEO;
        }
        else {
            Submit::$error['general'] = "Sorry, this format is not yet supported.";
            return false;
        }

        if($content_type == IMAGE){
            $thumb_source = PUBLIC_PATH."media/temp/".$time.".jpg";

            // Save image locally
            $time = time();
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
            $rawdata=curl_exec($ch);
            curl_close ($ch);

            $fp = fopen($thumb_source,'w');
            fwrite($fp, $rawdata); 
            fclose($fp);

        }
        else if($content_type == YOUTUBE){
            parse_str($url_info['query'], $params);
            $thumb_source = "http://img.youtube.com/vi/".$params['v']."/0.jpg";
        }
        else if($content_type == VINE){
            $vine = file_get_contents("http://vine.co/v/".$path['basename']);
            preg_match('/property="og:image" content="(.*?)"/', $vine, $matches);
            if($matches[1]) $thumb_source = $matches[1];
            else {
                Submit::$error['general'] = "There was a problem fetching the vine thumbnail.";
                return false;
            }
        }
        else if($content_type == VIMEO){
            $vimeo = file_get_contents($url);
            preg_match('/property="og:image" content="(.*?)"/', $vimeo, $matches);
            if($matches[1]) $thumb_source = $matches[1];
            else {
                Submit::$error['general'] = "There was a problem fetching the vimeo thumbnail.";
                return false;
            }
        }
        else if($content_type == DEVIANTART){
            // curl
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $dart = curl_exec($ch);
            curl_close($ch); 

            preg_match('/property="og:image" content="(.*?)"/', $dart, $matches);
            if($matches[1]) $thumb_source = $matches[1];
            else {
                Submit::$error['general'] = "There was a problem fetching the deviantart thumbnail.";
                return false;
            }

            $source = $url;
            $url = $matches[1];
            $content_type = IMAGE;
        }
        else if($host == "imgur.com"){
            $thumb_source = $url_info['scheme']."://".$host."/".$path['filename']."h.jpg";
        }

        $image = new SimpleImage($thumb_source);
        $image->square(200);
        if($content_type == IMAGE) unlink($thumb_source);
        $uid = uniqid();

        $sql = "INSERT INTO submissions (title, url, source, user_id, cat_id, content_type, host, unique_id, original_url) VALUES (:title, :url, :source, :user_id, :cat_id, :content_type, :host, :unique_id, :original_url)";
        $query = $this->db->prepare($sql);
        $query->execute(array(':title' => $title, ':url' => $url, ':source' => $source, ':user_id' => Session::get("user_id"), ':cat_id' => 0, ':content_type' => $content_type, ':host' => $host, ':unique_id' => $uid, ':original_url' => isset($original_url)?$original_url:$url));

        $item_id = $this->db->lastInsertId();

        // Insert tags into database if they don't already exist
        foreach($tags as $tag){
            $sql = "INSERT INTO tags (title) 
                    SELECT :tag FROM tags 
                    WHERE NOT EXISTS (SELECT * FROM tags 
                          WHERE title=:tag) LIMIT 1";
            $query = $this->db->prepare($sql);
            $query->execute(array(':tag' => $tag));
        }

        // Add tag listing of item to database
        foreach($tags as $tag){
            $tag_id = $this->getSingleValue("SELECT ID FROM tags WHERE title = '$tag'");
            $sql = "INSERT INTO tag_listing (tag_id, item_id) VALUES (:tag_id, :item_id)";
            $query = $this->db->prepare($sql);
            $query->execute(array(':tag_id' => $tag_id, ':item_id' => $item_id));
        }
        
        $image->save(PUBLIC_PATH . 'media/'.$uid.'.jpg', IMAGETYPE_JPEG, 100);

        return true;
    }

    /**
     * Flag an item for review by moderators
     */
    public function flagItem($item_id, $user_id)
    {

        if($this->isItemFlagged($item_id, $user_id))
            $sql = "DELETE FROM reports WHERE submission_id = :item_id AND user_id = :user_id";
        else
            $sql = "INSERT INTO reports (submission_id, user_id) VALUES (:item_id, :user_id)";

        $query = $this->db->prepare($sql);
        $query->execute(array(':item_id' => $item_id, ':user_id' => $user_id));

        return true;
    }

    /**
     * Hide item from public view
     */
    public function hideItem($item_id)
    {

        $sql = "UPDATE submissions SET visible = 0 WHERE ID = :item_id";

        $query = $this->db->prepare($sql);
        $query->execute(array(':item_id' => $item_id));

        return true;
    }

    /**
     * Make item visible to the public
     */
    public function showItem($item_id)
    {

        $sql = "UPDATE submissions SET visible = 1 WHERE ID = :item_id";

        $query = $this->db->prepare($sql);
        $query->execute(array(':item_id' => $item_id));

        return true;
    }

    /**
     * Add a like to an item by a user
     */
    public function likeItem($item_id, $user_id)
    {

        $value = 0;

        if($this->isItemLiked($item_id, $user_id)){
            $sql = "DELETE FROM likes WHERE submission_id = :item_id AND user_id = :user_id";
            $value = -1;
        }else{
            $sql = "INSERT INTO likes (submission_id, user_id) VALUES (:item_id, :user_id)";
            $value = 1;
        }

        $query = $this->db->prepare($sql);
        $query->execute(array(':item_id' => $item_id, ':user_id' => $user_id));

        // Increase experience of user
        $sql = "UPDATE users SET exp = exp + 1*$value WHERE ID = :user_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':user_id' => $user_id));

        // Increase experience of item owner
        $owner_id = $this->getSingleValue("SELECT user_id FROM submissions WHERE ID = $item_id");
        $sql = "UPDATE users SET exp = exp + 5*$value WHERE ID = :user_id";
        $query = $this->db->prepare($sql);
        if($owner_id != $user_id)
            $query->execute(array(':user_id' => $owner_id));


        return true;
    }

    /**
     * Add a bookmark to an item by a user
     */
    public function bookmarkItem($item_id, $user_id)
    {

        if($this->isItemBookmarked($item_id, $user_id))
            $sql = "DELETE FROM bookmarks WHERE submission_id = :item_id AND user_id = :user_id";
        else
            $sql = "INSERT INTO bookmarks (submission_id, user_id) VALUES (:item_id, :user_id)";

        $query = $this->db->prepare($sql);
        $query->execute(array(':item_id' => $item_id, ':user_id' => $user_id));

        return true;
    }

    /**
     * Determine if an item is flagged by a user
     */
    public function isItemFlagged($item_id, $user_id)
    {

        $query = $this->db->prepare("SELECT * FROM reports WHERE submission_id = :item_id AND user_id = :user_id");
        $query->execute(array(':item_id' => $item_id, ':user_id' => $user_id));

        return $query->rowCount();
    }

    /**
     * Determine if an item is liked by a user
     */
    public function isItemLiked($item_id, $user_id)
    {

        $query = $this->db->prepare("SELECT * FROM likes WHERE submission_id = :item_id AND user_id = :user_id");
        $query->execute(array(':item_id' => $item_id, ':user_id' => $user_id));

        return $query->rowCount();
    }

    /**
     * Determine if an item is bookmarked by a user
     */
    public function isItemBookmarked($item_id, $user_id)
    {

        $query = $this->db->prepare("SELECT * FROM bookmarks WHERE submission_id = :item_id AND user_id = :user_id");
        $query->execute(array(':item_id' => $item_id, ':user_id' => $user_id));

        return $query->rowCount();
    }

    /**
     * Get items that have been liked by a user
     */
    public function getLikedItems($user_id)
    {

        $query = $this->db->prepare("SELECT * FROM likes INNER JOIN submissions ON ID = submission_id WHERE likes.user_id = :user_id");
        $query->execute(array(':user_id' => $user_id));

        return $query->fetchAll();
    }

    /**
     * Get items that have been liked by a user
     */
    public function getBookmarkedItems($user_id)
    {

        $query = $this->db->prepare("SELECT * FROM bookmarks INNER JOIN submissions ON ID = submission_id WHERE bookmarks.user_id = :user_id");
        $query->execute(array(':user_id' => $user_id));

        return $query->fetchAll();
    }

    /**
     * Get tags for item
     */
    public function getTags($item_id)
    {

        $query = $this->db->prepare("SELECT title FROM tag_listing INNER JOIN tags ON tag_id = ID WHERE item_id = :item_id");
        $query->execute(array(':item_id' => $item_id));

        return $query->fetchAll();
    }

    /**
     * Get reports for item
     */
    public function getReports($item_id)
    {

        $query = $this->db->prepare("SELECT * FROM reports WHERE submission_id = :item_id");
        $query->execute(array(':item_id' => $item_id));

        return $query->rowCount();
    }

    /**
     * Remove all reports for item
     */
    public function clearReports($item_id)
    {

        $query = $this->db->prepare("DELETE FROM reports WHERE submission_id = :item_id");
        $query->execute(array(':item_id' => $item_id));

        return $query->rowCount();
    }

    /**
     * Remove all reports for item
     */
    public function deleteItem($item_id)
    {

        // delete thumbnail for item
        $date = $this->getSingleValue("SELECT date FROM submissions WHERE ID = $item_id");
        unlink(PUBLIC_PATH . 'media/'.strtotime($date).'.jpg');

        $query = $this->db->prepare("DELETE FROM submissions WHERE ID = :item_id; 
            DELETE FROM tag_listing WHERE item_id = :item_id;
            DELETE FROM likes WHERE submission_id = :item_id;
            DELETE FROM views WHERE submission_id = :item_id;");
        $query->execute(array(':item_id' => $item_id));

        return $query->rowCount();
    }



    
}
