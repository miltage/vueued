<?php

/**
 * Class Page
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Page extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index($page=null)
    {
        

        // load views. within the views we can echo out $songs and $amount_of_songs easily
        require 'application/views/_templates/header.php';        
        require 'application/views/_templates/green.php';
        require 'application/views/pages/header.php';
        if(file_exists('application/views/pages/'.$page.'.php'))
            require 'application/views/pages/'.$page.'.php';
        else
            require 'application/views/pages/404.php';
        require 'application/views/pages/footer.php';
        require 'application/views/_templates/footer.php';
    }
}
