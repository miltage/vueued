<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Submit extends Controller
{

    /**
     * Construct this object by extending the basic Controller class
     */
    function __construct()
    {
        parent::__construct();
    }

    public static $error = array();
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index() 
    {
        if(count($_POST)) $this->submit();
        
        require 'application/views/_templates/header.php';
        require 'application/views/_templates/green.php';
        require 'application/views/submit/header.php';
        require 'application/views/submit/index.php';
        require 'application/views/_templates/footer.php';
    }

    public function submit() 
    {
        Auth::handleLogin();

        if($this->insert())
            header('location: ' . URL );
    }

    public function insert(){

        // Halt process if user is banned
        $user_model = $this->loadModel("User");
        if($user_model->isBanned(Session::get("user_id"))){
            Submit::$error['general'] = "You are not allowed to post anything.";
            return;
        }
        
        $title = $_POST['title'];
        $url = $_POST['url'];
        $tags = $_POST['tags'];
        $source = $_POST['source'];

        if(!empty($title) && !empty($url) && !empty($tags)){
            $item_model = $this->loadModel("Item");
            if(count($item_model->getItemByURL($url)) > 0){
                Submit::$error['general'] = "That has already been submitted.";
                return false;
            }

            if($item_model->insertItem($title, $url, $tags, $source))
                return true;
        }
        else if(empty($url)){
            Submit::$error['url'] = "You definitely need to fill this in.";
        }
        if(empty($title)){
            Submit::$error['title'] = "You're not going to give it a title?";
        }
        if(empty($tags)){
            Submit::$error['tags'] = "You'll need this. This is important.";
        }

        return false;
    }

    public function remote()
    {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json; charset=UTF-8");

        ini_set("display_errors", 0);

        if(isset($_POST['sessid'])) session_id($_POST['sessid']);
        else{
            $login_model = $this->loadModel('Login');
            $login_successful = $login_model->login($_POST['username'], $_POST['password']);
        }

        if(Session::isLoggedIn()){
            $result = array("success" => $this->insert());
        }else{
            $result = array("success" => false);
            Submit::$error['general'] = "You are not logged in.";
        }

        if(!$result['success']){
            $result = array_merge($result, Submit::$error);
        }

        echo json_encode($result);

    }

    public function categories()
    {
        header("Content-Type: application/json; charset=UTF-8");

        $term = $_GET['term'];

        $sql = "SELECT name FROM categories WHERE name LIKE '$term%'";
        $query = $this->db->prepare($sql);
        $query->execute();

        $result = $query->fetchAll();
        foreach($result as $row){
            $row->value = $row->name;
            unset($row->name);
        }

        echo json_encode($result);
    }

    public function tags(){
        header("Content-Type: application/json; charset=UTF-8");

        $term = $_GET['term'];

        $sql = "SELECT title FROM tags WHERE title LIKE '$term%'";
        $query = $this->db->prepare($sql);
        $query->execute();

        $result = $query->fetchAll();
        foreach($result as $row){
            $row->value = $row->title;
            unset($row->title);
        }

        echo json_encode($result);
    }
}
