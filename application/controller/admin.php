<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Admin extends Controller
{
    public static $error = array();
    
    public function index($view=null)
    {

        Auth::handleLogin();

        $user_model = $this->loadModel("User");
        $item_model = $this->loadModel('Item');
        $comment_model = $this->loadModel('Comment');

        if(!$user_model->isAdmin(Session::get("user_id")))
            header("Location: ".URL);

        $reported_items = $item_model->getReportedItems();
        $reported_comments = $comment_model->getReportedComments();

        $parsedown = new Parsedown();
        $loadChildren = false;

        $total_users = $user_model->getTotalUsers();
        $total_items = $item_model->getTotalItems();
        $total_comments = $comment_model->getTotalComments();
        $last_user = $user_model->getUserInfo($total_users);
        $last_active_users = $user_model->getLastActive();

        // load views.
        require 'application/views/_templates/header.php';
        require 'application/views/_templates/green.php';
        require 'application/views/admin/header.php';
        require 'application/views/admin/index.php';
        require 'application/views/admin/footer.php';
        require 'application/views/_templates/footer.php';
    }

    

}
