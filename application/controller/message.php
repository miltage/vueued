<?php

/**

    Class Message

    Handles the loading of user messages

*/
class Message extends Controller
{
    public static $error = array();
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index($unique_id, $view=null)
    {

        $message_model = $this->loadModel('Message');
        $thread_id = $message_model->getThreadID($unique_id);
        $canRead = false;

        if($thread_id){
            if($canRead = $message_model->canRead($thread_id, Session::get("user_id"))){
                $messages = $message_model->getThreadMessages($thread_id);
                $events = $message_model->getThreadEvents($thread_id);

                $thread_items = array_merge($messages, $events);
                usort($thread_items, function($a, $b){
                    return $a->date > $b->date;
                });

                $thread = $message_model->getThread($thread_id);
                $last_read = $message_model->getLastRead($thread_id, Session::get("user_id"));
                $message_model->setLastRead($thread_id, Session::get("user_id"));
                $users = $message_model->getThreadUsers($thread_id);
                $isOwner = $message_model->isOwner($thread_id, Session::get("user_id"));

                if($isOwner && count($_POST)) $this->submitUsers($thread_id, $unique_id);

                $parsedown = new Parsedown();
            }
        }
        

        // load views.
        require 'application/views/_templates/header.php';
        require 'application/views/_templates/green.php';
        require 'application/views/message/header.php';
        if($thread_id && $canRead){
            require 'application/views/message/sidebar.php';
            if($view == "users" && $isOwner)
                require 'application/views/message/users.php';
            else if($view == "users" && !$isOwner)
                require 'application/views/message/403.php';
            else
                require 'application/views/message/index.php';
        }
        else if($thread_id && !$canRead){
            require 'application/views/message/403.php';
        }
        else{
            require 'application/views/message/404.php';
        }
        require 'application/views/message/footer.php';
        require 'application/views/_templates/footer.php';
    }

    public function send($unique_id)
    {
        header("Content-Type: application/json; charset=UTF-8");

        $content = isset($_POST['content'])?$_POST['content']:null;
        $content = htmlentities($content);
        $message_model = $this->loadModel('Message');
        $thread_id = $message_model->getThreadID($unique_id);
        echo $message_model->sendMessage($thread_id, Session::get("user_id"), $content);
    }

    public function submitUsers($thread_id, $unique_id) 
    {
        Auth::handleLogin();
        
        $recipients = $_POST['recipients'];

        if(!empty($recipients)){
            $message_model = $this->loadModel("Message");
            $recipients = explode(",", $recipients);
            if($message_model->editThreadUsers($thread_id, $recipients))
                header('location: ' . URL .'message/'.$unique_id.'/');
        }
        else if(empty($recipients)){
            Message::$error['recipients'] = "You'd only be speaking to yourself.";
        }
    }

}
