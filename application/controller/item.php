<?php

/**
 * Class Item
 *  
 *
 */
class Item extends Controller
{

    /**
     * PAGE: index
     * This returns the html for an item overview
     */
    public function index($item_id)
    {        

        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $json = $ext == "json";

        if($json)
            header("Content-Type: application/json; charset=UTF-8");

        $user_model = $this->loadModel('User');
        
        $item_model = $this->loadModel('Item');
        $item = $item_model->loadItem($item_id);
        if(!$item){
            require 'application/views/_templates/header.php';
            require 'application/views/item/header.php';
            include 'application/views/item/404.php';
            require 'application/views/_templates/footer.php';
            return;
        }

        $item->ID = $item_id;
        $url_info = parse_url($item->url);        
        $path = pathinfo($url_info['path']);

        // Determine item content and load
        if($item->content_type == IMAGE){
            $item->content = "<img src=\"".$item->url."\" />";
        }
        else if($item->content_type == YOUTUBE){
            parse_str($url_info['query'], $params);
            $item->content = "<iframe width=\"800\" height=\"450\" src=\"http://www.youtube.com/embed/".$params['v']."?rel=0&amp;showinfo=0&amp;autoplay=1\" frameborder=\"0\" allowfullscreen></iframe>";
        }
        else if($item->content_type == VIDEO){
            ob_start();
            require 'application/views/item/video.php';
            $item->content = ob_get_clean();
        }
        else if($item->content_type == VINE){
            $item->content = "<iframe src=\"".$item->url."/embed/simple?audio=1\" width=\"480\" height=\"480\" frameborder=\"0\"></iframe><script src=\"https://platform.vine.co/static/scripts/embed.js\"></script>";
        }
        else if($item->content_type == VIMEO){
            $item->content = "<iframe src=\"https://player.vimeo.com/video/".$path['filename']."?color=ffffff&byline=0&portrait=0&badge=0\" width=\"800\" height=\"450\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
        }
    
        $item->liked = $item_model->isItemLiked($item_id, Session::get("user_id"));
        $item->saved = $item_model->isItemBookmarked($item_id, Session::get("user_id"));
        $item->flagged = $item_model->isItemFlagged($item_id, Session::get("user_id"));
        $item->numReports = $item_model->getReports($item_id);

        if(!$json) require 'application/views/_templates/header.php';
        if(!$json) require 'application/views/item/header.php';
        if(isset($item)) include 'application/views/item/index.php';
        if(!$json) require 'application/views/_templates/footer.php';
    }

    public function like($item_id) 
    {
        if(!Session::isLoggedIn()){
            echo json_encode(array("success" => false));
            exit;
        }

        $item_model = $this->loadModel("Item");
        $item_model->likeItem($item_id, Session::get("user_id"));

        echo json_encode(array("success" => true));
    }

    public function bookmark($item_id) 
    {
        if(!Session::isLoggedIn()){
            echo json_encode(array("success" => false));
            exit;
        }

        $item_model = $this->loadModel("Item");
        $item_model->bookmarkItem($item_id, Session::get("user_id"));

        echo json_encode(array("success" => true));
    }

    public function flag($item_id) 
    {
        if(!Session::isLoggedIn()){
            echo json_encode(array("success" => false));
            exit;
        }

        $item_model = $this->loadModel("Item");
        $item_model->flagItem($item_id, Session::get("user_id"));

        echo json_encode(array("success" => true));
    }

    public function clear($item_id)
    {
        Auth::handleLogin();

        $item_model = $this->loadModel("Item");
        $user_model = $this->loadModel("User");
        if(!$user_model->isAdmin(Session::get("user_id"))) exit;

        $item_model->clearReports($item_id);
    }

    public function hide($item_id)
    {
        Auth::handleLogin();

        $user_model = $this->loadModel('User');
        if(!$user_model->isAdmin(Session::get("user_id"))) return;

        $item_model = $this->loadModel("Item");
        $item_model->hideItem($item_id);
    }

    public function show($item_id)
    {
        Auth::handleLogin();

        $user_model = $this->loadModel('User');
        if(!$user_model->isAdmin(Session::get("user_id"))) return;

        $item_model = $this->loadModel("Item");
        $item_model->showItem($item_id);
    }

    public function delete($item_id)
    {
        Auth::handleLogin();

        $user_model = $this->loadModel('User');
        if(!$user_model->isAdmin(Session::get("user_id"))) return;

        $item_model = $this->loadModel("Item");
        $item_model->deleteItem($item_id);
    }

    public function fetchComments($item_id, $parent=0){
        $item_model = $this->loadModel("Item");
        $user_model = $this->loadModel("User");
        $comment_model = $this->loadModel('Comment');
        $comments = $comment_model->getItemComments($item_id, $parent);

        $parsedown = new Parsedown();

        if($parent && count($comments)) echo "<ul>";

        $loadChildren = true;

        foreach($comments as $comment){
            if($comment->removed && $comment_model->getNumChildren($comment->ID) == 0) continue;
            include 'application/views/item/comment.php';
        }

        if($parent && count($comments)) echo "</ul>";
    }

   
}
