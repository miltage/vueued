<?php

/**
 * Class Songs
 * This is a demo class.
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Grid extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/songs/index
     */
    public function index()
    {
        header("Content-Type: application/json; charset=UTF-8");
        
        $item_model = $this->loadModel('Item');
    }

    public function latest()
    {
        header("Content-Type: application/json; charset=UTF-8");
        
        $item_model = $this->loadModel('Item');
        $items = $item_model->getLatestItems();

        foreach($items as $item){
            include 'application/views/grid/item.php';
        }
    }

    
}
