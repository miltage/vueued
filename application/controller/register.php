<?php

/**
 * Class Login
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Register extends Controller
{
    /**
     * Construct this object by extending the basic Controller class
     */
    function __construct()
    {
        parent::__construct();
    }

    public static $error = array();
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index() 
    {
        if(count($_POST)) $this->register();

        require 'application/views/_templates/header.php';
        require 'application/views/_templates/green.php';
        require 'application/views/register/header.php';
        require 'application/views/register/index.php';
        require 'application/views/_templates/footer.php';
    }

    public function register(){

        ini_set("display_errors", 1);

        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $password2 = $_POST['password2'];

        if(!isset($username) || empty($username))
            Register::$error['username'] = "You kinda need this part.";

        if(strlen($username) < 2)
            Register::$error['username'] = "Your username is too short.";

        if(strlen($username) > 64)
            Register::$error['username'] = "Your username is too long.";

        if(preg_match('/^[a-z\d]$/i', $username))
            Register::$error['username'] = "Your username contains illegal characters.";

        if(!isset($email) || empty($email))
            Register::$error['email'] = "Don't forget this. You need this for password recovery.";

        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            Register::$error['email'] = "Your email address is invalid.";            

        if(!isset($password) || empty($password))
            Register::$error['password'] = "This is important too.";

        if(!isset($password2) || empty($password2))
            Register::$error['password2'] = "Gonna have to ask you for this.";

        if(strlen($password) < 6)
            Register::$error['password'] = "Your password needs to be at least 6 characters long.";

        if($password != $password2)
            Register::$error['password2'] = "Your passwords did not match.";

        if(empty($username) || empty($email) || empty($password) || empty($password2)
            || strlen($password)<6 || $password != $password2 || preg_match('/^[a-z\d]$/i', $username)) return false;

        // run the register() method in the login-model, put the result in $register_successful (true or false)
        $login_model = $this->loadModel('Login');
        // perform the login method, put result (true or false) into $register_successful
        $register_successful = $login_model->registerNewUser($username, $email, $password);

        // check register status
        if($register_successful) {
            // if YES, then move user to home page
            if($login_model->login($username, $password))
                header('location: ' . URL);
            else
                header('location: ' . URL . 'login/');
        }
    }

    /**
     * Login with cookie
     */
    function loginWithCookie()
    {
        // run the loginWithCookie() method in the login-model, put the result in $login_successful (true or false)
        $login_model = $this->loadModel('Login');
        $login_successful = $login_model->loginWithCookie();

        if ($login_successful) {
            header('location: ' . URL);
        } else {
            // delete the invalid cookie to prevent infinite login loops
            $login_model->deleteCookie();
            // if NO, then move user to login/index (login form) (this is a browser-redirection, not a rendered view)
            header('location: ' . URL . 'login/');
        }
    }

    
}
