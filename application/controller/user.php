<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class User extends Controller
{
    public static $error = array();
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index($username, $view=null)
    {
        if(count($_POST)) $this->submit();

        $user_model = $this->loadModel("User");
        $user_id = $user_model->getUserID($username);
        $user_info = $user_model->getUserInfo($user_id);

        $item_model = $this->loadModel('Item');

        // load views. within the views we can echo out $songs and $amount_of_songs easily
        require 'application/views/_templates/header.php';
        if($user_id){
            if($view == "edit"){
                require 'application/views/_templates/green.php';
                require 'application/views/user/edit.php';
            }
            else {
                require 'application/views/user/header.php';
                require 'application/views/user/sidebar.php';
                if($view == "liked") require 'application/views/user/liked.php';
                else if($view == "saved") require 'application/views/user/bookmarked.php';
                else require 'application/views/user/index.php';
            }
        }else{
            require 'application/views/user/404.php';
        }
        require 'application/views/_templates/footer.php';
    }

    public function submit() 
    {
        Auth::handleLogin();
        
        $website = $_POST['website'];
        $twitter = $_POST['twitter'];
        $facebook = $_POST['facebook'];
        $instagram = $_POST['instagram'];
        $youtube = $_POST['youtube'];
        $vimeo = $_POST['vimeo'];
        $deviantart = $_POST['deviantart'];
        $soundcloud = $_POST['soundcloud'];
        $tumblr = $_POST['tumblr'];
        $dribbble = $_POST['dribbble'];

        $user_model = $this->loadModel("User");
        $user_model->setProfileLink(Session::get("user_id"), "website", $website);
        $user_model->setProfileLink(Session::get("user_id"), "twitter", $twitter);
        $user_model->setProfileLink(Session::get("user_id"), "facebook", $facebook);
        $user_model->setProfileLink(Session::get("user_id"), "instagram", $instagram);
        $user_model->setProfileLink(Session::get("user_id"), "youtube", $youtube);
        $user_model->setProfileLink(Session::get("user_id"), "vimeo", $vimeo);
        $user_model->setProfileLink(Session::get("user_id"), "deviantart", $deviantart);
        $user_model->setProfileLink(Session::get("user_id"), "soundcloud", $soundcloud);
        $user_model->setProfileLink(Session::get("user_id"), "tumblr", $tumblr);
        $user_model->setProfileLink(Session::get("user_id"), "dribbble", $dribbble);

        header('location: ' . URL .'user/'.strtolower(Session::get("username"))."/");
    }

}
