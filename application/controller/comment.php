<?php

/**
 * Class Comment
 *  
 *
 */
class Comment extends Controller
{

    /**
     * PAGE: index
     * This returns the html for an item overview
     */
    public function index($comment_id)
    {        

        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $json = $ext == "json";

        if($json)
            header("Content-Type: application/json; charset=UTF-8");

        $user_model = $this->loadModel('User');
        
        $item_model = $this->loadModel('Item');
        $item = $item_model->loadItem($item_id);
        if(!$item){
            require 'application/views/_templates/header.php';
            require 'application/views/item/header.php';
            include 'application/views/item/404.php';
            require 'application/views/_templates/footer.php';
            return;
        }

        $item->ID = $item_id;
        $url_info = parse_url($item->url);        

        if(!$json) require 'application/views/_templates/header.php';
        if(!$json) require 'application/views/item/header.php';
        if(isset($item)) include 'application/views/item/index.php';
        if(!$json) require 'application/views/_templates/footer.php';
    }

    public function report($comment_id) 
    {
        if(!Session::isLoggedIn()){
            echo json_encode(array("success" => false));
            exit;
        }

        $comment_model = $this->loadModel("Comment");
        $comment_model->report($comment_id, Session::get("user_id"));

        echo json_encode(array("success" => true));
    }

    public function get($comment_id) 
    {
        header("Content-Type: application/json; charset=UTF-8");

        $comment_model = $this->loadModel("Comment");
        $comment = $comment_model->getComment($comment_id);

        echo json_encode($comment);
    }

    public function edit($comment_id) 
    {
        if(!Session::isLoggedIn()){
            echo json_encode(array("success" => false));
            exit;
        }

        header("Content-Type: application/json; charset=UTF-8");

        $comment_model = $this->loadModel("Comment");
        $rows = $comment_model->editComment($comment_id, $_POST['source']);

        $comment = $comment_model->getComment($comment_id);
        $parsedown = new Parsedown();

        echo json_encode(array("success" => $rows>0, "content" => $parsedown->text($comment->content)));
    }

    public function post($item_id) 
    {

        $user_model = $this->loadModel("User");
        if(!Session::isLoggedIn() || empty($_POST['content']) || $user_model->isBanned(Session::get("user_id"))){
            echo json_encode(array("success" => false));
            exit;
        }

        $comment_model = $this->loadModel("Comment");
        $comment = $comment_model->postComment($item_id, Session::get("user_id"), $_POST['content'], $_POST['parent']);

        $parsedown = new Parsedown();
        $loadChildren = false;
        ob_start();
        include 'application/views/item/comment.php';
        
        echo json_encode(array("success" => true, "result" => ob_get_clean()));
    }

    public function clear($comment_id)
    {
        Auth::handleLogin();

        $comment_model = $this->loadModel("Comment");
        $user_model = $this->loadModel("User");
        if(!$user_model->isAdmin(Session::get("user_id"))) exit;

        $comment_model->clearReports($comment_id);
    }

    public function remove($comment_id)
    {
        Auth::handleLogin();

        $comment_model = $this->loadModel("Comment");
        $user_model = $this->loadModel("User");
        if(!$user_model->isAdmin(Session::get("user_id")) && $comment_model->getAuthor($comment_id) != Session::get("user_id")) exit;

        $comment_model->removeComment($comment_id);
        $comment_model->clearReports($comment_id);
    }

    

   
}
