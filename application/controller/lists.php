<?php

/**
 * Class Lists
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Lists extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index($view=null)
    {
        $list_model = $this->loadModel('List');
        $lists = $list_model->getUserLists(Session::get("user_id"));

        // load views. 
        require 'application/views/_templates/header.php';
        require 'application/views/lists/header.php';
        require 'application/views/lists/index.php';
        require 'application/views/_templates/footer.php';
    }

    public function activate($list_id){
        Session::set("active_list", $list_id);
    }

    public function addItem($list_id, $item_name){
        $list_model = $this->loadModel('List');
        $item_name = str_replace("_", " ", $item_name);
        $item_id = $list_model->getSingleValue("SELECT ID FROM tags WHERE title = '$item_name'");
        echo $list_model->addItemToList($list_id, $item_id);
    }

    public function deleteItem($list_id, $item_name){
        $list_model = $this->loadModel('List');
        $item_id = $list_model->getSingleValue("SELECT ID FROM tags WHERE title = '$item_name'");
        echo $list_model->removeItemFromList($list_id, $item_id);
    }

    public function rename($list_id, $list_title){
        $list_title = str_replace("_", " ", $list_title);
        $list_model = $this->loadModel('List');
        $list_model->renameList($list_id, $list_title);
    }

    public function add(){
        $list_model = $this->loadModel('List');
        $list_id = $list_model->addList();
        $list = $list_model->getList($list_id);
        require 'application/views/lists/list.php';
    }

    public function delete($list_id){
        $list_model = $this->loadModel('List');
        echo $list_model->deleteList($list_id);
    }

    public function get($list_id){
        header("Content-Type: application/json; charset=UTF-8");
        $list_model = $this->loadModel('List');
        $list = $list_model->getList($list_id);
        require 'application/views/lists/list.php';

    }
}
