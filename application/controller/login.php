<?php

/**
 * Class Login
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Login extends Controller
{
    /**
     * Construct this object by extending the basic Controller class
     */
    function __construct()
    {
        parent::__construct();
    }

    public $error = array();
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index() 
    {
        if(count($_POST)) $this->login();
        
        require 'application/views/_templates/header.php';
        require 'application/views/_templates/green.php';
        require 'application/views/login/header.php';
        require 'application/views/login/index.php';
        require 'application/views/_templates/footer.php';
    }

    public function login(){

        ini_set("display_errors", 1);

        $username = strtolower($_POST['username']);
        $password = $_POST['password'];

        if(!isset($username) || empty($username))
            $this->error['username'] = "You kinda need this part.";

        if(!isset($password) || empty($password))
            $this->error['password'] = "This is important too.";

        if(empty($username) || empty($password)) return false;

        // run the login() method in the login-model, put the result in $login_successful (true or false)
        $login_model = $this->loadModel('Login');
        // perform the login method, put result (true or false) into $login_successful
        $login_successful = $login_model->login($username, $password);

        // check login status
        if($login_successful) {
            // if YES, then move user to home page
            header('location: ' . URL );
        }
    }

    /**
     * Login with cookie
     */
    function loginWithCookie()
    {
        // run the loginWithCookie() method in the login-model, put the result in $login_successful (true or false)
        $login_model = $this->loadModel('Login');
        $login_successful = $login_model->loginWithCookie();

        if ($login_successful) {
            header('location: ' . URL);
        } else {
            // delete the invalid cookie to prevent infinite login loops
            $login_model->deleteCookie();
            // if NO, then move user to login/index (login form) (this is a browser-redirection, not a rendered view)
            header('location: ' . URL . 'login/');
        }
    }

    
}
