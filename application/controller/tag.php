<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Tag extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index($tag_name, $view=null)
    {

        $tag_model = $this->loadModel("Tag");
        $tag_id = $tag_model->getTagID($tag_name);

        $item_model = $this->loadModel('Item');

        // load views. within the views we can echo out $songs and $amount_of_songs easily
        require 'application/views/_templates/header.php';
        if($tag_id){
            require 'application/views/tag/header.php';   
            if($view == "latest") require 'application/views/tag/latest.php';
            else if($view == "greatest") require 'application/views/tag/greatest.php';
            else require 'application/views/tag/index.php';
        }else{
            require 'application/views/tag/404.php';
        }
        require 'application/views/_templates/footer.php';
    }

}
