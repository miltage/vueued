<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Cat extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index($cat_name, $view=null)
    {

        $cat_model = $this->loadModel("Cat");
        $cat_id = $cat_model->getCatID($cat_name);

        $item_model = $this->loadModel('Item');

        // load views. within the views we can echo out $songs and $amount_of_songs easily
        require 'application/views/_templates/header.php';
        if($cat_id){
            require 'application/views/cat/header.php';
            if($view == "latest") require 'application/views/cat/latest.php';
            else if($view == "greatest") require 'application/views/cat/greatest.php';
            else require 'application/views/cat/index.php';
        }else{
            require 'application/views/cat/404.php';
        }
        require 'application/views/_templates/footer.php';
    }

}
