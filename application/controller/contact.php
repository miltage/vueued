<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Contact extends Controller
{
    public static $error = array();
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index()
    {
        if(count($_POST)) $this->submit();
        require 'application/views/_templates/header.php';
        require 'application/views/_templates/green.php';
        require 'application/views/contact/header.php';
        require 'application/views/contact/index.php';
        require 'application/views/_templates/footer.php';
    }

    public function submit() 
    {
        $email = isset($_POST['email'])?$_POST['email']:"";
        $name = isset($_POST['name'])?$_POST['name']:"";;
        $subject = $_POST['subject'];
        $message = $_POST['message'];

        if(isset($_SESSION['user_logged_in'])){
            $name = Session::get('username');
            $email = Session::get('email');
        }

        if(!empty($email) && !empty($name) && !empty($message)){
            require_once LIBS_PATH . 'PHPMailer/PHPMailerAutoload.php';

            $mail = new PHPMailer;

            $mail->From = 'mailer@vueued.com';
            $mail->FromName = 'Mailer';
            $mail->addAddress('info@vueued.com');
            $mail->addReplyTo($email, $name);
            $mail->isHTML(true);

            $mail->Subject = $subject;
            $mail->Body    = $message;

            if(!$mail->send()) {
                Contact::$error['general'] = "There was a problem sending your message.<br>Mailer Error: " . $mail->ErrorInfo;
            } else {
                Contact::$error['general'] = "Thanks! Your message has been successfully sent.";
                unset($_POST['message']);
            }

        }
        else if(empty($message)){
            Contact::$error['message'] = "You need to put something in here.";
        }
    }
}
