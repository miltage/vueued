<?php

/**

    Class Inbox

    A place to display all the user's messages, both read and unread

*/
class Inbox extends Controller
{
    public static $error = array();
    
    public function index($view=null)
    {

        $message_model = $this->loadModel('Message');

        if(count($_POST)) $this->submit();
        
        $messages = $message_model->getUserMessages(Session::get("user_id"));

        // load views.
        require 'application/views/_templates/header.php';
        require 'application/views/_templates/green.php';
        require 'application/views/inbox/header.php';
        require 'application/views/inbox/sidebar.php';
        if($view == "compose") require 'application/views/inbox/compose.php';
        else require 'application/views/inbox/index.php';
        require 'application/views/inbox/footer.php';
        require 'application/views/_templates/footer.php';
    }

    public function submit() 
    {
        Auth::handleLogin();
        
        $recipients = $_POST['recipients'];
        $subject = $_POST['subject'];
        $message = $_POST['message'];

        if(!empty($recipients) && !empty($subject) && !empty($message)){
            $message_model = $this->loadModel("Message");
            $recipients = explode(",", $recipients);
            if($message_model->composeMessage($recipients, $subject, $message, Session::get("user_id")))
                header('location: ' . URL .'inbox/');
        }
        else if(empty($recipients)){
            Inbox::$error['recipients'] = "Somebody needs to get this message.";
        }
        if(empty($subject)){
            Inbox::$error['subject'] = "What is this message all about then?";
        }
        if(empty($message)){
            Inbox::$error['message'] = "Nothing to say?";
        }
    }

    public function delete($unique_id) 
    {
        Auth::handleLogin();

        // detect ajax request
        $ajax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest');
        
        $message_model = $this->loadModel('Message');
        $thread_id = $message_model->getThreadID($unique_id);
        $result = $message_model->removeUserFromThread($thread_id, Session::get("user_id"));

        $message_model->createThreadEvent($thread_id, Session::get("username")." left the thread.");

        if($ajax) echo $result;
        else header('location: ' . URL . 'inbox/');
    }

    public function users(){
        header("Content-Type: application/json; charset=UTF-8");

        $term = $_GET['term'];

        $sql = "SELECT username FROM users WHERE username LIKE '$term%'";
        $query = $this->db->prepare($sql);
        $query->execute();

        $result = $query->fetchAll();
        foreach($result as $row){
            $row->value = $row->username;
            unset($row->username);
        }

        echo json_encode($result);
    }

}
