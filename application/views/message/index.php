
<h2><?php echo $thread->subject; ?></h2>
<?php 
	foreach($thread_items as $item):
		if(property_exists($item, "user_id")): // is not event item
			$unread = TimeUtil::timeCompare($item->date, $last_read);
?>
<div class="message <?php echo $unread?'unread':''; ?>">
	<span class="username"><a href="<?php echo URL."user/".strtolower($item->username)."/"; ?>"><?php echo $item->username; ?></a></span>
	<small class="time" title="<?php echo TimeUtil::formatTime($item->date); ?>"><?php echo TimeUtil::timeSince($item->date)." ago"; ?></small>
	<div class="content">
		<?php echo $parsedown->text($item->content); ?>
	</div>
</div>
<?php else: ?>
	<div class="message">
		<span class="message-event">
			<?php echo $item->content; ?>
		</span>
	</div>
<?php 
	endif;
	endforeach; 
?>

<textarea class="message"></textarea>
<button id="send" style="float:right">Send</button>
<small class="status"></small>

<?php 
	/* Keep later for message events
		<div class="message">
			<span class="message-event">
				Miltage added User to the message.
			</span>
		</div>
	*/
?>
