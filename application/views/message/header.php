	<script src="<?php echo URL; ?>public/js/messaging.js" type="text/javascript"></script>
	<script src="<?php echo URL; ?>public/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="<?php echo URL; ?>public/js/tag-it.min.js" type="text/javascript" charset="utf-8"></script>

	<link href="<?php echo URL; ?>public/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo URL; ?>public/css/jquery.tagit.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		$(function(){
			$("input[name='recipients']").tagit({singleField: true, removeConfirmation: true, allowSpaces: true, 
				autocomplete: {delay: 0, minLength: 2, source: "<?php echo URL; ?>inbox/users/"}
			});
		});

		var unique_id = "<?php echo $unique_id; ?>";
	</script>
</div>
<div id="page-wrapper">