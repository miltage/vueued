<div id="sidebar">
	<?php if($view == null): ?>
	<a href="<?php echo URL."inbox/"; ?>">return to inbox</a>
	<p>Users in this thread:</p>
	<ul>
		<?php foreach($users as $user): ?>
		<li><a href="<?php echo URL."user/".strtolower($user->username)."/"; ?>"><?php echo $user->username; ?></a></li>
		<?php endforeach; ?>
	</ul>
	<ul id="user-links">
		<?php if($isOwner): ?>
		<li><a href="<?php echo URL."message/".$unique_id."/users/"; ?>">edit users</a></li>
		<?php endif; ?>
		<li><a id="delete" href="<?php echo URL."inbox/delete/".$unique_id."/"; ?>">delete</a></li>
	</ul>
	<?php else: ?>
		<a href="<?php echo URL."message/".$unique_id."/"; ?>">return to message</a>
	<?php endif; ?>
</div>