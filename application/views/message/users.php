
<h2>Manage users</h2>
<p>Here you can decide exactly who can read the message thread.</p>

<?php
	$recipients = "";
	foreach($users as $user)
		if($user->ID != $thread->owner)
			$recipients .= $user->username.",";
?>

<form action="" method="post" autocomplete="off">
	<small class="error"><?php if(isset(Message::$error['general'])) echo Message::$error['general']; ?></small>
	<fieldset>
		<label>Recipients</label>
		<small class="error"><?php if(isset(Message::$error['recipients'])) echo Message::$error['recipients']; ?></small>
		<input type="text" name="recipients" value="<?php echo isset($_REQUEST['recipients'])?$_REQUEST['recipients']:$recipients; ?>" />
		<small>Usernames separated by commas.</small>
	</fieldset>
	<button style="float: right">Save</button>
</form>

