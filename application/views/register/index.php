
<div id="wrapper">
	<form action="" method="post" autocomplete="off" style="max-width: 500px; margin: 0 auto;">
		<p>Make sure you read and understand <a href="#">our disclaimer</a> before you create your account.</p>
		<fieldset>
			<label>Username</label>
			<small class="error"><?php if(isset(Register::$error['username'])) echo Register::$error['username']; ?></small>
			<input type="text" name="username" value="<?php echo isset($_POST['username'])?$_POST['username']:""; ?>" />
		</fieldset>
		<fieldset>
			<label>Email Address</label>
			<small class="error"><?php if(isset(Register::$error['email'])) echo Register::$error['email']; ?></small>
			<input type="text" name="email" value="<?php echo isset($_POST['email'])?$_POST['email']:""; ?>" />
		</fieldset>
		<fieldset>
			<label>Password</label>
			<small class="error"><?php if(isset(Register::$error['password'])) echo Register::$error['password']; ?></small>
			<input type="password" name="password" value="<?php echo isset($_POST['password'])?$_POST['password']:""; ?>" />
		</fieldset>
		<fieldset>
			<label>Confirm Password</label>
			<small class="error"><?php if(isset(Register::$error['password2'])) echo Register::$error['password2']; ?></small>
			<input type="password" name="password2" value="<?php echo isset($_POST['password2'])?$_POST['password2']:""; ?>" />
		</fieldset>
	    <button class="big" style="margin-top: 20px">register</button>
	</form>
</div>