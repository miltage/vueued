<h1>Rules</h1>
<p>Make sure to follow these guidelines when submitting content and using the site in general. 
If there is anything you are unsure about, please use the <a href="<?php echo URL; ?>contact/">contact page</a> to get in touch.</p>
<p>Above all else, use common sense. If you wouldn't do it IRL, don't do it on here.</p>

<h3>No spam</h3>
<p>Please don't clog up the system with advertisements. Self-promotion is alright in moderation.<br>Spammers will be blocked.</p>

<h3>No personal information</h3>
<p>Don't post personal details about yourself or others.</p>

<h3>No hateful comments</h3>
<p>Let's try to keep discussions civil.</p>

<h3>No pornography</h3>
<p>Submissions of a sensitive nature need to be marked 'nsfw'.</p>

<p><i>Rules are subject to change.</i></p>