<h1>About</h1>

<p>VUEUED (pronounced "viewed") is a content sharing platform with a focus on visual media.</p>

<h3>How does it work?</h3>

<p>Users submit images and videos to the site and the content that is engaged with the most (views, likes) rises to the top of the pile.</p>
<p>Content is categorized by tags, so users can build lists of tags which they can use to sort through the submissions and display only the items they want to see.</p>

<h3>What can I submit?</h3>

<p>Currently, the following file formats are supported: </p>

<ul class="image-list">
	<li><img src="<?php echo URL; ?>public/img/social/Polaroid.png" /> Image files (jpg, png, gif)</li>
	<li><img src="<?php echo URL; ?>public/img/social/Youtube.png" /> YouTube</li>
	<li><img src="<?php echo URL; ?>public/img/social/Vimeo.png" /> Vimeo</li>
	<li><img src="<?php echo URL; ?>public/img/social/Vine.png" /> Vines</li>
	<li><img src="<?php echo URL; ?>public/img/social/DeviantART.png" /> DeviantArt</li>
</ul>

<p>If you'd like support added for a certain media type, suggest it!</p>

<!-- <p>Support for the following formats will be implemented soon: </p>

<ul class="image-list">
	<li></li>
</ul> -->

<h3>Why did you make this?</h3>

<p>Wow, okay.</p>

<h3>Is it supposed to be making this noise?</h3>

<p>The site is in beta, which means a lot of the features you're using are experimental. If you find something that doesn't work or just have an idea of how it can be approved, 
	please don't hesitate to <a href="<?php echo URL."pages/contact/"; ?>">let me know</a>. Well, you should hesitate a little. Consider if it's really worth bothering me. </p>