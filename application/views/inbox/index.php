
<h2>Inbox</h2>
<?php if(count($messages)): ?>
<ul id="messages">
	<?php 
		foreach($messages as $message): 
		    $last_read = $message_model->getLastRead($message->ID, Session::get("user_id"));
			$unread = TimeUtil::timeCompare($message->last_activity, $last_read);
	?>
	<a href="<?php echo URL."message/".$message->unique_id."/"; ?>"><li <?php if($unread) echo "class=\"unread\""; ?> data-id="<?php echo $message->unique_id; ?>"><?php echo $message->subject; ?><span class="delete icon icon-cross" title="delete"></span></li></a>
	<?php endforeach; ?>
</ul>
<?php else: ?>
	Your inbox is empty.
<?php endif; ?>
