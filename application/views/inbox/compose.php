
<h2>Compose Message</h2>

<form action="" method="post" autocomplete="off">
	<small class="error"><?php if(isset(Inbox::$error['general'])) echo Inbox::$error['general']; ?></small>
	<fieldset>
		<label>Recipients</label>
		<small class="error"><?php if(isset(Inbox::$error['recipients'])) echo Inbox::$error['recipients']; ?></small>
		<input type="text" name="recipients" value="<?php echo isset($_REQUEST['recipients'])?$_REQUEST['recipients']:""; ?>" />
		<small>Usernames separated by commas.</small>
	</fieldset>
	<fieldset>
		<label>Subject</label>
		<small class="error"><?php if(isset(Inbox::$error['subject'])) echo Inbox::$error['subject']; ?></small>
		<input type="text" name="subject" value="<?php echo isset($_POST['subject'])?$_POST['subject']:""; ?>" />
	</fieldset>
	<fieldset>
		<label>Message</label>
		<small class="error"><?php if(isset(Inbox::$error['message'])) echo Inbox::$error['message']; ?></small>
		<textarea name="message"><?php echo isset($_POST['message'])?$_POST['message']:""; ?></textarea>
	</fieldset>
    <button style="float: right">Send</button>
</form>

