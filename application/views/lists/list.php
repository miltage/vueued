<li id="<?php echo $list->ID; ?>">
	<div class="buttons">
		<span title="delete list" class="delete-list icon-square-cross"></span>
	</div>
	<span class="title"><input type="text" value="<?php echo $list->title; ?>" /></span>
	<ul class="items">
		<?php
			$list_items = $list_model->getListItems($list->ID);
			foreach($list_items as $list_item){
				echo "<li><a href=\"#\">".$list_item->title."</a><span title=\"delete item\" class=\"delete-list-item icon-cross\"></span></li>";
			}
		?>
		<li><span title="add item" class="add-list-item icon-plus"></span></li>
	</ul>
</li>
