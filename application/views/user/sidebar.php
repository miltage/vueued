<div id="sidebar">
	<h2><?php echo $user_info->username; ?></h2>
	<small><?php echo TimeUtil::timeSince($user_info->date_joined)." old"; ?></small>
	<ul id="social-links">
		<?php if(!empty($user_info->website)): ?><a href="<?php echo $user_info->website; ?>" target="_blank"><img src="<?php echo URL; ?>public/img/social/Globe.png" /></a><?php endif; ?>
		<?php if(!empty($user_info->twitter)): ?><a href="<?php echo $user_info->twitter; ?>" target="_blank"><img src="<?php echo URL; ?>public/img/social/Twitter.png" /></a><?php endif; ?>
		<?php if(!empty($user_info->facebook)): ?><a href="<?php echo $user_info->facebook; ?>" target="_blank"><img src="<?php echo URL; ?>public/img/social/FaceBook.png" /></a><?php endif; ?>
		<?php if(!empty($user_info->deviantart)): ?><a href="<?php echo $user_info->deviantart; ?>" target="_blank"><img src="<?php echo URL; ?>public/img/social/DeviantART.png" /></a><?php endif; ?>
		<?php if(!empty($user_info->instagram)): ?><a href="<?php echo $user_info->instagram; ?>" target="_blank"><img src="<?php echo URL; ?>public/img/social/Instagram.png" /></a><?php endif; ?>
		<?php if(!empty($user_info->youtube)): ?><a href="<?php echo $user_info->youtube; ?>" target="_blank"><img src="<?php echo URL; ?>public/img/social/Youtube.png" /></a><?php endif; ?>
		<?php if(!empty($user_info->vimeo)): ?><a href="<?php echo $user_info->vimeo; ?>" target="_blank"><img src="<?php echo URL; ?>public/img/social/Vimeo.png" /></a><?php endif; ?>
		<?php if(!empty($user_info->tumblr)): ?><a href="<?php echo $user_info->tumblr; ?>" target="_blank"><img src="<?php echo URL; ?>public/img/social/Tumblr.png" /></a><?php endif; ?>
		<?php if(!empty($user_info->soundcloud)): ?><a href="<?php echo $user_info->soundcloud; ?>" target="_blank"><img src="<?php echo URL; ?>public/img/social/SoundCloud.png" /></a><?php endif; ?>
		<?php if(!empty($user_info->dribbble)): ?><a href="<?php echo $user_info->dribbble; ?>" target="_blank"><img src="<?php echo URL; ?>public/img/social/Dribbble.png" /></a><?php endif; ?>
	</ul>
	<ul id="user-links">
		<?php if($user_info->ID == Session::get("user_id")): ?>
		<li><a href="<?php echo URL."user/".strtolower($user_info->username)."/edit/"; ?>">edit profile</a></li>
		<?php else: ?>
		<li><a href="<?php echo URL."inbox/compose/"; ?>">send message</a></li>
		<li><a href="<?php echo URL."user/".strtolower($user_info->username)."/block/"; ?>">block user</a></li>
		<?php endif; ?>
	</ul>
</div>