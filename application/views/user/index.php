
<div id="grid">
	<?php

		$items = $item_model->getUserItems($user_id);

		if(!count($items)) echo "There doesn't appear to be anything here...";

        foreach($items as $item){
            include 'application/views/grid/item.php';
        }
        
	?>
    
</div>
