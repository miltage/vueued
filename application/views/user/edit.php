<div id="wrapper">
	<form action="" method="post" autocomplete="off">
		<p>Here you can add links to your social media profiles, if you so wish.</p>
		<small class="error"><?php if(isset(User::$error['general'])) echo User::$error['general']; ?></small>
		<fieldset>
			<label>Website</label>
			<small class="error"><?php if(isset(User::$error['website'])) echo User::$error['website']; ?></small>
			<input type="text" name="website" value="<?php echo isset($_POST['website'])?$_POST['website']:$user_info->website; ?>" />
			<small></small>
		</fieldset>
		<fieldset>
			<label>Facebook</label>
			<small class="error"><?php if(isset(User::$error['facebook'])) echo User::$error['facebook']; ?></small>
			<input type="text" name="facebook" value="<?php echo isset($_POST['facebook'])?$_POST['facebook']:$user_info->facebook; ?>" />
			<small></small>
		</fieldset>
		<fieldset>
			<label>Twitter</label>
			<small class="error"><?php if(isset(User::$error['twitter'])) echo User::$error['twitter']; ?></small>
			<input type="text" name="twitter" value="<?php echo isset($_POST['twitter'])?$_POST['twitter']:$user_info->twitter; ?>" />
			<small></small>
		</fieldset>
		<fieldset>
			<label>Instagram</label>
			<small class="error"><?php if(isset(User::$error['instagram'])) echo User::$error['instagram']; ?></small>
			<input type="text" name="instagram" value="<?php echo isset($_POST['instagram'])?$_POST['instagram']:$user_info->instagram; ?>" />
			<small></small>
		</fieldset>
		<fieldset>
			<label>DeviantArt</label>
			<small class="error"><?php if(isset(User::$error['deviantart'])) echo User::$error['deviantart']; ?></small>
			<input type="text" name="deviantart" value="<?php echo isset($_POST['deviantart'])?$_POST['deviantart']:$user_info->deviantart; ?>" />
			<small></small>
		</fieldset>
		<fieldset>
			<label>Tumblr</label>
			<small class="error"><?php if(isset(User::$error['tumblr'])) echo User::$error['tumblr']; ?></small>
			<input type="text" name="tumblr" value="<?php echo isset($_POST['tumblr'])?$_POST['tumblr']:$user_info->tumblr; ?>" />
			<small></small>
		</fieldset>
		<fieldset>
			<label>YouTube</label>
			<small class="error"><?php if(isset(User::$error['youtube'])) echo User::$error['youtube']; ?></small>
			<input type="text" name="youtube" value="<?php echo isset($_POST['youtube'])?$_POST['youtube']:$user_info->youtube; ?>" />
			<small></small>
		</fieldset>
		<fieldset>
			<label>Vimeo</label>
			<small class="error"><?php if(isset(User::$error['vimeo'])) echo User::$error['vimeo']; ?></small>
			<input type="text" name="vimeo" value="<?php echo isset($_POST['vimeo'])?$_POST['vimeo']:$user_info->vimeo; ?>" />
			<small></small>
		</fieldset>
		<fieldset>
			<label>Soundcloud</label>
			<small class="error"><?php if(isset(User::$error['soundcloud'])) echo User::$error['soundcloud']; ?></small>
			<input type="text" name="soundcloud" value="<?php echo isset($_POST['soundcloud'])?$_POST['soundcloud']:$user_info->soundcloud; ?>" />
			<small></small>
		</fieldset>
		<fieldset>
			<label>Dribbble</label>
			<small class="error"><?php if(isset(User::$error['dribbble'])) echo User::$error['dribbble']; ?></small>
			<input type="text" name="dribbble" value="<?php echo isset($_POST['dribbble'])?$_POST['dribbble']:$user_info->dribbble; ?>" />
			<small></small>
		</fieldset>
	    <button class="big">submit</button>
	</form>
</div>