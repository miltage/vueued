
<div id="wrapper">
	<form action="" method="post" autocomplete="off">
		<p>Please read <a href="<?php echo URL."page/rules/"; ?>">the rules</a> before submitting content.</p>
		<small class="error"><?php if(isset(Submit::$error['general'])) echo Submit::$error['general']; ?></small>
		<fieldset>
			<label>Title</label>
			<small class="error"><?php if(isset(Submit::$error['title'])) echo Submit::$error['title']; ?></small>
			<input type="text" name="title" value="<?php echo isset($_POST['title'])?$_POST['title']:""; ?>" />
			<small>Call it something.</small>
		</fieldset>
		<fieldset>
			<label>URL</label>
			<small class="error"><?php if(isset(Submit::$error['url'])) echo Submit::$error['url']; ?></small>
			<input type="text" name="url" value="<?php echo isset($_POST['url'])?$_POST['url']:""; ?>" />
			<small>Where on the web your content is located.</small>
		</fieldset>
		<fieldset>
			<label>Tags</label>
			<small class="error"><?php if(isset(Submit::$error['tags'])) echo Submit::$error['tags']; ?></small>
			<input type="text" name="tags" value="<?php echo isset($_POST['tags'])?$_POST['tags']:""; ?>" />
			<small>Enter tags that describe your submission, separated by commas.</small>
		</fieldset>
		<fieldset>
			<label>Source (optional)</label>
			<input type="text" name="source" value="<?php echo isset($_POST['source'])?$_POST['source']:""; ?>" />
			<small>If your content is best accredited to another individual, <br>you can provide a source.</small>
		</fieldset>
	    <button class="big">submit</button>
	</form>
</div>