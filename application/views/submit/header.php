
	<script src="<?php echo URL; ?>public/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="<?php echo URL; ?>public/js/tag-it.min.js" type="text/javascript" charset="utf-8"></script>

	<link href="<?php echo URL; ?>public/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo URL; ?>public/css/jquery.tagit.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		$(function(){
			//$("input[name='category']").autocomplete({ source: "<?php echo URL; ?>/submit/categories/"});
			$("input[name='tags']").tagit({singleField: true, removeConfirmation: true, allowSpaces: true, 
				autocomplete: {delay: 0, minLength: 2, source: "<?php echo URL; ?>submit/tags/"}
			});
		});		
	</script>
</div>