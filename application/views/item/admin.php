<small class="admin">
	<?php if($item->numReports) echo "<p class=\"numReports\">".$item->numReports." report".($item->numReports>1?"s":"")."</p>"; ?>
	<?php 
		if($item->visible) echo "<a class=\"hide\" href=\"#\">hide</a>";
		else echo "<a class=\"show\" href=\"#\">show</a>";;
	?>

	<a class="delete" href="#">delete</a>
	<?php if($item->numReports): ?> <a class="clear" href="#">clear reports</a> <?php endif; ?>
</small>