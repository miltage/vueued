<li><span><i class="collapse icon-marquee-minus"></i>
	<?php if($comment->removed): ?>
	[removed]
	<?php else: ?>
	<a href="<?php echo URL."user/".strtolower($comment->username)."/"; ?>"><?php echo $comment->username; ?></a>
	<?php endif; ?>
    <a class="time" href="#" title="<?php echo TimeUtil::formatTime($comment->date_posted); ?>">
    	<?php 
    		echo TimeUtil::timeSince($comment->date_posted)." ago";
    		if(!empty($comment->date_edited) && $comment->date_edited > 0){
    			echo " * (Last edited ".TimeUtil::timeSince($comment->date_edited)." ago)";
    		}
    	?>
    </a></span>
    <span class="comment">
    	<span>
    <?php 
    	if($comment->removed) 
    		echo "[Comment removed]"; 
    	else 
    		echo $parsedown->text($comment->content);
    ?>
    	</span>
    <?php
    	if(Session::isLoggedIn() && !$comment->removed): ?>
	    <span class="links">
	    	<a data-parent="<?php echo $comment->ID; ?>" class="reply" href="#">reply</a>
	    <?php if($comment->user_id == Session::get("user_id")): ?>
	    	<a href="" data-id="<?php echo $comment->ID; ?>" class="edit">edit</a><a href="" data-id="<?php echo $comment->ID; ?>" class="delete">delete</a>
	    <?php else: ?>
	    	<a href="" data-id="<?php echo $comment->ID; ?>" class="report"><?php echo $comment_model->hasBeenReported($comment->ID, Session::get("user_id"))?"unreport":"report"; ?></a>
	    <?php endif; ?>
        <?php if($user_model->isAdmin(Session::get("user_id"))): ?>
            <a href="" data-id="<?php echo $comment->ID; ?>" class="remove">remove</a><a href="" data-id="<?php echo $comment->ID; ?>" class="clear">clear reports</a>
        <?php endif; ?>
	    </span>
		<?php endif; ?>
<?php
    if($loadChildren)
        $this->fetchComments($item_id, $comment->ID);
?>
</span></li>