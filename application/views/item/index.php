<div id="content-wrapper">
	<h2><?php echo $item->title; ?></h2>
	<div id="content-frame">
		<div id="content">
			<?php if($json): ?>
			<span id="prev-item" class="nav"><span class="icon-arrow-left"></span></span>
			<span id="next-item" class="nav"><span class="icon-arrow-right"></span></span>
			<?php endif; ?>
			<div><?php echo $item->content; ?></div>
		</div>
	</div>
	<div id="source">
		<?php
			if(!empty($item->source) && strpos($item->source, "http") !== false) 
				echo "<a href=\"".$item->source."\" target=\"_blank\">".$item->source."</a>";
			else if(!empty($item->source)) 
				echo $item->source; 
		?>
	</div>
	<div id="content-buttons">
		<span class="icon icon-flag <?php if($item->flagged) echo "active"; ?>" data-title="Report"></span>
		<span class="icon icon-ribbon <?php if($item->saved) echo "active"; ?>" data-title="Save"></span>
		<span class="icon icon-heart <?php if($item->liked) echo "active"; ?>" data-title="Like"></span>
		<span class="title">Like</span>
		<?php if($json): ?><span class="icon-cross" style="float: right"></span><?php endif; ?>
		<small class="time" title="<?php echo TimeUtil::formatTime($item->date); ?>"><a href="<?php echo URL."item/".$item->ID."/"; ?>"><?php echo TimeUtil::timeSince($item->date)." ago"; ?></a></small>
		<small class="info">submitted by <a href="<?php echo URL."user/".strtolower($item->username)."/"; ?>"><?php echo $item->username; ?></a></small>
		<?php 
			if($user_model->isAdmin(Session::get("user_id")))
				include 'admin.php'; 
		?>
		<ul class="tags">
			<?php
				$tags = $item_model->getTags($item->ID);
				foreach($tags as $tag):
					$title = str_replace(" ", "_", $tag->title);
			?>
			<li><a href="<?php echo URL."tag/".strtolower($title)."/"; ?>"><?php echo $tag->title; ?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
<div id="comments-wrapper">
	<div id="comment-buttons">
		<?php if(Session::isLoggedIn()): ?><button data-parent="0" class="comment">Add Comment</button>
		<?php else: ?><small>Log in to post comments.</small>
		<?php endif; ?></div>
	<ul id="comments">
		<?php $this->fetchComments($item->ID); ?>
	</ul>
</div>

<?php 
// set up item id for ajax functions
if(!$json): ?>
<script type="text/javascript">
	itemID = <?php echo $item->ID; ?>;
</script>
<?php endif; ?>