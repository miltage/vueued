
<div id="grid">
	<?php

        if(Session::get('user_logged_in') && Session::get('active_list') > 0)
        	$items = $item_model->getBestFromList(Session::get('active_list'), $list_model);
        else if(Session::get('active_list') < 0)
        	$items = $item_model->getBestFromDefault(Session::get('active_list'));
        else if(!Session::get('user_logged_in'))
        	$items = $item_model->getBestFromDefault(0);
        else
        	$items = $item_model->getBest();
        
        foreach($items as $item){
            include 'application/views/grid/item.php';
        }
	?>
    
</div>
