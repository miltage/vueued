<div id="sidebar">

	<ul class="list">
		<li data-id="0" class="<?php echo !Session::get("active_list")?"active":""; ?>">everything</li>
	</ul>

	<ul class="list">
		<li data-id="-1" class="<?php echo Session::get("active_list")==-1?"active":""; ?>">images</li>
		<li data-id="-2" class="<?php echo Session::get("active_list")==-2?"active":""; ?>">videos</li>
		<li data-id="-3" class="<?php echo Session::get("active_list")==-3?"active":""; ?>">music</li>
		<li data-id="-4" class="<?php echo Session::get("active_list")==-4?"active":""; ?>">movies</li>
	</ul>

<?php
	if (Session::get('user_logged_in') == true):
?>

	<ul class="list">
		<?php
			$lists = $list_model->getUserLists(Session::get("user_id"));
			foreach($lists as $list){
				echo "<li data-id=\"".$list->ID."\"".(Session::get("active_list")==$list->ID?"class=\"active\"":"").">".$list->title."</li>";
			}
		?>
	</ul>

	<?php if(Session::get("active_list") > 0): ?>
	<span>in this list:</span>
	<ul>
		<?php
			$list_items = $list_model->getListItems(Session::get("active_list"));
			foreach($list_items as $list_item){
				$title = str_replace(" ", "_", $list_item->title);
				echo "<li><a href=\"".URL."tag/".strtolower($title)."/\">".$list_item->title."</a></li>";
			}
		?>
	</ul>
	<?php endif; ?>

	<a href="<?php echo URL."lists/"; ?>">manage lists</a>
<?php else: ?>
    
<?php endif; ?>

</div>