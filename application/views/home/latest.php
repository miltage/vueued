
<div id="grid">
	<?php
	
        if(Session::get('user_logged_in') && Session::get('active_list') > 0)
            $items = $item_model->getLatestItemsFromList(Session::get('active_list'), $list_model);
        else if(Session::get('active_list') < 0)
            $items = $item_model->getLatestItemsFromDefault(Session::get('active_list'));
        else if(!Session::get('user_logged_in'))
            $items = $item_model->getLatestItemsFromDefault(0);
        else
            $items = $item_model->getLatestItems();
        
        foreach($items as $item){
            include 'application/views/grid/item.php';
        }
	?>
    
</div>
