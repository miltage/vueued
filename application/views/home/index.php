
<div id="grid">
	<?php

        if(Session::get('user_logged_in') && Session::get('active_list') > 0)
        	$items = $item_model->getFreshFromList(Session::get('active_list'), $list_model);
        else if(Session::get('active_list') < 0)
        	$items = $item_model->getFreshFromDefault(Session::get('active_list'));
        else if(!Session::get('user_logged_in'))
        	$items = $item_model->getFreshFromDefault(0);
        else
        	$items = $item_model->getFresh();

        foreach($items as $item){
            include 'application/views/grid/item.php';
        }
	?>
    
</div>
