<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>vueued</title>
    <meta name="keywords" content="content-sharing, images, video, gifs, funny">
    <meta name="description" content="A content-sharing platform with a focus on visual media.">
    <link href="<?php echo URL; ?>public/css/normalize.css" rel="stylesheet" />
    <link href="<?php echo URL; ?>public/css/style.css" rel="stylesheet" />
    <link href="<?php echo URL; ?>public/css/responsive.css" rel="stylesheet" />
    <link href="<?php echo URL; ?>public/css/feather.css" rel="stylesheet" />
    <link rel="icon" type="image/png" href="<?php echo URL; ?>public/img/favicon.ico">
    <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?php echo URL; ?>public/js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="<?php echo URL; ?>public/js/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo URL; ?>public/js/system.js" type="text/javascript"></script>
</head>
<body>
    <?php include('analyticstracking.php'); ?>
    <div id="header">
        <a href="<?php echo URL; ?>"><span id="logo"></span></a>
    <?php if (Session::get('user_logged_in') == true):
            $message_model = $this->loadModel("Message");
            $new_mail = $message_model->hasUnreadMessages(Session::get("user_id"));
        ?>
        <span id="account"><?php echo Session::get('username'); ?></span>
        <ul id="buttons">
            <a href="<?php echo URL."inbox/"; ?>" title="Inbox"><li class="button <?php echo $new_mail?'notice':''; ?>"><i class="icon-mail"></i></li></a>
            <a href="<?php echo URL."submit/"; ?>" title="Submit"><li class="button"><i class="icon-plus"></i></li></a>
        </ul>
        <ul id="actions">
            <a href="<?php echo URL."inbox/"; ?>"><li><i class="icon-mail"></i>Inbox</li></a>
            <a href="<?php echo URL."user/".strtolower(Session::get('username'))."/"; ?>"><li><i class="icon-head"></i>Profile</li></a>
            <a href="<?php echo URL."submit/"; ?>"><li><i class="icon-plus"></i>New Submission</li></a>
            <a href="<?php echo URL."lists/"; ?>"><li><i class="icon-menu"></i>Lists</li></a>
            <a href="<?php echo URL."user/".strtolower(Session::get('username'))."/liked/"; ?>"><li><i class="icon-heart"></i>Liked</li></a>
            <a href="<?php echo URL."user/".strtolower(Session::get('username'))."/saved/"; ?>"><li><i class="icon-ribbon"></i>Saved</li></a>
            <a href="<?php echo URL; ?>logout/"><li><i class="icon-esc"></i>Log Out</li></a>
        </ul>
    <?php else: ?>
        <a id="login" href="<?php echo URL; ?>login/"><span id="account">Register / Login</span></a>
    <?php endif; ?>
