
<div id="wrapper">
	<form action="" method="post" autocomplete="off" style="max-width: 500px; margin: 0 auto;">
		<p>If you already have an account, you can login here.</p>
		<p>If not, you can create one <a href="<?php echo URL.'register/'; ?> ">here</a>.</p>
		<fieldset>
			<label>Username</label>
			<small class="error"><?php if(isset($this->error['username'])) echo $this->error['username']; ?></small>
			<input type="text" name="username" value="<?php echo isset($_POST['username'])?$_POST['username']:""; ?>" />
		</fieldset>
		<fieldset>
			<label>Password</label>
			<small class="error"><?php if(isset($this->error['password'])) echo $this->error['password']; ?></small>
			<input type="password" name="password" />
		</fieldset>
		<fieldset style="text-align: left">
			<label for="rememberme"><input type="checkbox" id="rememberme" name="rememberme" /> Remember Me</label>
		</fieldset>
	    <button class="big" style="margin-top: 20px">go</button>
	</form>
</div>