<h1>Admin</h1>

<p>Look at all these glorious stats!</p>

<div class="info-block">
	<h3>Registered Users</h3>
	<span><?php echo $total_users; ?></span>
</div>

<div class="info-block">
	<h3>Submissions</h3>
	<span><?php echo $total_items; ?></span>
</div>

<div class="info-block">
	<h3>Comments</h3>
	<span><?php echo $total_comments; ?></span>
</div>

<div class="info-block">
	<h3>Latest User</h3>
	<a href="<?php echo URL."user/".strtolower($last_user->username)."/"; ?>"><?php echo $last_user->username; ?></a>
</div>

<div class="info-block">
	<h3>Last Active</h3>
	<?php foreach($last_active_users as $user): ?>
	<a href="<?php echo URL."user/".strtolower($user->username)."/"; ?>" title="<?php echo TimeUtil::timeSince($user->last_activity)." ago"; ?>"><?php echo $user->username; ?></a>
	<?php endforeach; ?>
</div>

<div style="clear:both; height: 50px;"></div>

<h3>Reported Items</h3>

<div id="grid">
	<?php
        foreach($reported_items as $item){
            include 'application/views/grid/item.php';
        }
	?>    
</div>

<h3>Reported Comments</h3>

<div id="comments-wrapper">
	<ul id="comments">
	<?php
	    foreach($reported_comments as $comment){
	        include 'application/views/item/comment.php';
	    }
	?>
	</ul>
</div>

