
<div id="wrapper">
	<form action="" method="post" autocomplete="off">
		<p>Want to get in contact with me? You can use the form below to drop me a line.</p>
		<p>Alternatively, you can tweet <a href="http://twitter.com/vueued">@vueued</a>.</p>
		<small class="error"><?php if(isset(Contact::$error['general'])) echo Contact::$error['general']; ?></small>
		<?php if(!isset($_SESSION['user_logged_in'])): ?>
		<fieldset>
			<label>Your email address</label>
			<small class="error"><?php if(isset(Contact::$error['email'])) echo Contact::$error['email']; ?></small>
			<input type="text" name="email" value="<?php echo isset($_POST['email'])?$_POST['email']:""; ?>" />
			<small>If you'd like a reply.</small>
		</fieldset>
		<fieldset>
			<label>Your name</label>
			<small class="error"><?php if(isset(Contact::$error['name'])) echo Contact::$error['name']; ?></small>
			<input type="text" name="name" value="<?php echo isset($_POST['name'])?$_POST['name']:""; ?>" />
			<small>If you'd like to be addressed in that reply.</small>
		</fieldset>
		<?php endif; ?>
		<fieldset>
			<label>Subject</label>
			<small class="error"><?php if(isset(Contact::$error['subject'])) echo Contact::$error['subject']; ?></small>
			<select name="subject" class="large">
				<option>I have a suggestion</option>
				<option>I have a question</option>
				<option>I have a problem</option>
				<option>I just want to say hi</option>
			</select>
		</fieldset>
		<fieldset>
			<label>Message</label>
			<small class="error"><?php if(isset(Contact::$error['message'])) echo Contact::$error['message']; ?></small>
			<textarea name="message"><?php echo isset($_POST['message'])?$_POST['message']:""; ?></textarea>
		</fieldset>
	    <button class="big">submit</button>
	</form>
</div>