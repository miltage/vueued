<?php
	if(!isset($icons)) $icons = array("camera", "video", "video", "video", "video");
?>
<figure data-id="<?php echo $item->ID; ?>">		        
	<div style="background-image: url('<?php echo URL."public/media/".($item->unique_id).".jpg"; ?>');"></div>
    <figcaption>
        <p><?php echo $item->title; ?></p>
        <a href="<?php echo $item->url; ?>" data-type="image" data-id="<?php echo $item->ID; ?>"></a>
        <small><span class="icon-<?php echo $icons[$item->content_type]; ?>"></span></small>
    </figcaption>         
</figure>