<?php

/**
 * TimeUtil class
 *
 * This is a helper class for custom time-related functions.
 */
class TimeUtil
{
    /**
     * Calculates how much time has passed since specific point of time
     */
    public static function timeSince($time)
    {
        date_default_timezone_set('Africa/Johannesburg');
        $date = new DateTime($time);
        $diff = $date->diff(new DateTime());
        if($diff->m > 0) return $diff->m." month".($diff->m!=1?"s":"");
        if($diff->d > 0) return $diff->d." day".($diff->d!=1?"s":"");
        if($diff->h > 0) return $diff->h." hour".($diff->h!=1?"s":"");
        if($diff->i > 0) return $diff->i." minute".($diff->i!=1?"s":"");
        return $diff->s." second".($diff->s!=1?"s":"");
    }

    /**
     * Calculates how much time has passed since specific point of time in seconds
     */
    public static function timeCompare($time1, $time2)
    {
        $t1 = new DateTime($time1);
        $t2 = new DateTime($time2);
        return $t1 > $t2;
    }

    /**
     * Formats the time into something nice and understandable
     */
    public static function formatTime($time)
    {
        $date = new DateTime($time);
        return $date->format("Y-m-d H:i:s");
    }

    
}
