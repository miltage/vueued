<?php

/**
 * This is the "base model class". All other "real" models extend this class.
 */
class Model
{
    /**
     * Every model needs a database connection, passed to the model
     * @param object $db A PDO database connection
     */
    function __construct($db) {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    /**
     *  Utility functions 
    */

    function getSingleValue($query){
        $q = $this->db->query($query);
        return $q->fetchColumn();
    }
}
