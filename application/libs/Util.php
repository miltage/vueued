<?php

/**
 * Util class
 *
 * This is a helper class for general convenience functions.
 */
class Util
{
    /**
     * Calculates how much time has passed since specific point of time
     */
    public static function parseDomain($url) {
		$pieces = parse_url($url);
		$domain = isset($pieces['host']) ? $pieces['host'] : '';
		if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
			return $regs['domain'];
		}
		return false;
	}

	/**
     * Formats an array into an English list
     * e.g. array(1, 2, 3) => 1, 2 and 3.
     */
    public static function formatList($list) {
		$result = "";
		if(count($list)<2) return $list[0];
		for($i=0; $i<count($list)-1; $i++){
			$result .= $list[$i].", ";
		}
		$result = rtrim($result, ", ");
		$result .= " and ".$list[count($list)-1];
		return $result;
	}

    
}