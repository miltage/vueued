# vueued

A content aggregator with a focus on visual media (ie. images and videos)

# about

This is a personal project, undertaken with the intention of learning new frameworks and programming concepts

I built it on top of [this bare-bones MVC framework](https://github.com/panique/mini) I found.

In addition to the main project, I also wrote a bot that would scrape and post content from Reddit in `/bot/` and I made a browser extension for Chrome in `/extension/` so you could easily post content from other sites.

I had the site running for a few months before moving on to other projects and archiving the code.