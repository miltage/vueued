// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/**
 * Get the current URL.
 *
 * @param {function(string)} callback - called when the URL of the current tab
 *   is found.
 **/
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var tab = tabs[0];

    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    var url = tab.url;

    // tab.url is only available if the "activeTab" permission is declared.
    // If you want to see the URL of other tabs (e.g. after removing active:true
    // from |queryInfo|), then the "tabs" permission is required to see their
    // "url" properties.
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url);
  });

  // Most methods of the Chrome extension APIs are asynchronous. This means that
  // you CANNOT do something like this:
  //
  // var url;
  // chrome.tabs.query(queryInfo, function(tabs) {
  //   url = tabs[0].url;
  // });
  // alert(url); // Shows "undefined", because chrome.tabs.query is async.
}

/**
 * @param {string} searchTerm - Search term for Google Image search.
 * @param {function(string,number,number)} callback - Called when an image has
 *   been found. The callback gets the URL, width and height of the image.
 * @param {function(string)} errorCallback - Called when the image is not found.
 *   The callback gets a string that describes the failure reason.
 */
function getImageUrl(searchTerm, callback, errorCallback) {
  // Google image search - 100 searches per day.
  // https://developers.google.com/image-search/
  var searchUrl = 'https://ajax.googleapis.com/ajax/services/search/images' +
    '?v=1.0&q=' + encodeURIComponent(searchTerm);
  var x = new XMLHttpRequest();
  x.open('GET', searchUrl);
  // The Google image search API responds with JSON, so let Chrome parse it.
  x.responseType = 'json';
  x.onload = function() {
    // Parse and process the response from Google Image Search.
    var response = x.response;
    if (!response || !response.responseData || !response.responseData.results ||
        response.responseData.results.length === 0) {
      errorCallback('No response from Google Image search!');
      return;
    }
    var firstResult = response.responseData.results[0];
    // Take the thumbnail instead of the full image to get an approximately
    // consistent image size.
    var imageUrl = firstResult.tbUrl;
    var width = parseInt(firstResult.tbWidth);
    var height = parseInt(firstResult.tbHeight);
    console.assert(
        typeof imageUrl == 'string' && !isNaN(width) && !isNaN(height),
        'Unexpected respose from the Google Image Search API!');
    callback(imageUrl, width, height);
  };
  x.onerror = function() {
    errorCallback('Network error.');
  };
  x.send();
}

function submitContent(vars, callback, errorCallback){
  var submitURL = "http://vueued.com/submit/remote";
  var x = new XMLHttpRequest();
  x.open('POST', submitURL);
  x.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  x.send("title="+vars.title+"&url="+vars.url+"&tags="+vars.tags+"&source="+vars.source+"&sessid="+sessid);
  x.onload = function(){
    var response = x.response;
    callback(JSON.parse(response));
  };
  x.onerror = function(){
    errorCallback("Network error.");
  };
  x.send();
}

function renderStatus(statusText) {
  document.getElementById('status').textContent = statusText;
}

document.addEventListener('DOMContentLoaded', function() {


  getCurrentTabUrl(function(url) {
    document.getElementsByName('url')[0].value = url;
  });

  document.getElementById("submitButton").addEventListener("click", function() {

        var x = document.querySelectorAll("input");
        for (i = 0; i < x.length; i++) {
            x[i].disabled = true;
        }

        var y = document.querySelectorAll("small");
        for (i = 0; i < y.length; i++) {
            y[i].innerHTML = "";
        }


        // Put the image URL in Google search.
        renderStatus('Sending request to server...');

        var vars = {url: document.getElementsByName('url')[0].value, title: document.getElementsByName('title')[0].value, tags: document.getElementsByName('tags')[0].value, source: document.getElementsByName('source')[0].value};

        submitContent(vars, function(data) {

            renderStatus("");

            console.log(data.success);

            if(data.success){
                renderStatus("Submission successful!");
                for (i = 0; i < x.length; i++) {
                    x[i].value = "";
                }
            }else{
                if(data.general != null) renderStatus(data.general);
                if(data.title != null) document.querySelector("small[for='title']").innerHTML = data.title;
                if(data.url != null) document.querySelector("small[for='url']").innerHTML = data.url;
                if(data.tags != null) document.querySelector("small[for='tags']").innerHTML = data.tags;
                if(data.source != null) document.querySelector("small[for='source']").innerHTML = data.source;
            }

            for (i = 0; i < x.length; i++) {
                x[i].disabled = false;
            }

        }, 
        function(errorMessage) {
            renderStatus('There was a problem contacting the server. ' + errorMessage);
        });

  });
});

var port = chrome.extension.connect({name: "vueued"});
var sessid;
port.onMessage.addListener(function(data) {
    var sessid = data;
    console.log("sessid = "+sessid);
});
