var path = "/vueued/";

$(function(){
	$(document).on("click", "button#send", function(event){
		var content = $("textarea").val();
		if(!content.length){
			$("small.status").html("You forgot to type something to send!");
			$("textarea").focus();
			return false;
		}
		$(this).prop("disabled", true);
		$("small.status").html("Sending...");
		$.post(path+"message/send/"+unique_id+"/", {content: content}, function(data){
			if(data.success){
				$("small.status").html("Message sent.");
				window.location.href = window.location.href;
			}else{
				$("small.status").html(data.result);
			}
		}, "json");
	});

	$(document).on("click", "span.delete", function(event){
		event.preventDefault();
		event.stopPropagation();
		var c = confirm("Are you sure you want to delete this message?");
		if(c){
			var message = $(this).parent().parent();
			var unique_id = $(this).parent().attr("data-id");
			$.post(path+"inbox/delete/"+unique_id+"/", function(data){
				if(data.success){
					message.slideUp();
				}else{
					alert(data.result);
				}
			}, "json");
		}
	});

	$(document).on("click", "a#delete", function(event){
		var c = confirm("Are you sure you want to delete this message?");
		return c;
	});

});