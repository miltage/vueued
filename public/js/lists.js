var path = "/vueued/";

$(function(){
	$(document).on("click", "span.add-list-item", function(event){
		$(this).replaceWith("<input type=\"text\" name=\"new_item\"/><span title=\"add item\" class=\"save-list-item icon-check\"></span>");
		init();
	});

	$(document).on("click", "span.save-list-item", function(event){
		var item = $(this).prev("input").val().replace(" ", "_");
		var list = $(this).parent().parent().parent().attr("id");
		var t = $(this);
		if(!item.length){
			alert("Enter a category name.");
			return;
		}

		$.post(path+"lists/additem/"+list+"/"+item+"/", function(data){
			if(data.success){
				t.prev("input").remove();
				t.parent().parent().append("<li><span title=\"add item\" class=\"add-list-item icon-plus\"></span></li>");
				t.replaceWith("<a href=\"#\">"+data.result+"</a><span title=\"delete item\" class=\"delete-list-item icon-cross\"></span>");
			}else{
				alert(data.result);
			}
		}, "json");

	});

	$(document).on("click", "span.delete-list-item", function(event){
		var item = $(this).prev("a").html();
		var list = $(this).parent().parent().parent().attr("id");
		var t = $(this);
		$.post(path+"lists/deleteitem/"+list+"/"+item+"/", function(data){
			if(data.success){
				t.parent().remove();
			}else{
				alert(data.result);
			}
		}, "json");
	});

	$(document).on("click", "span.delete-list", function(event){
		if(!confirm("Are you sure you want to delete this list?")) return false;
		var list = $(this).parent().parent().attr("id");
		var t = $(this);
		$.post(path+"lists/delete/"+list+"/", function(data){
			if(data.success){
				t.parent().parent().slideUp();
			}else{
				alert(data.result);
			}
		}, "json");
	});

	var delayTimer;
	$(document).on("keyup", "span.title input", function(){
		var title = $(this).val().replace(" ", "_");
		var list = $(this).parent().parent().attr("id");
		clearTimeout(delayTimer);
	    delayTimer = setTimeout(function() {
	        $.post(path+"lists/rename/"+list+"/"+title+"/");
	    }, 1000);
	});

	$(document).on("click", "#add-list", function(){
		$.post(path+"lists/add/", function(data){
			$("#add-list").before(data);
		});
	});

	$(document).on("keypress", "input[name='new_item']", function(e){
	    var regex = new RegExp("^[a-zA-Z0-9 ]+$");
	    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	    if (regex.test(str)) {
	        return true;
	    }

	    e.preventDefault();
	    return false;
	});

});