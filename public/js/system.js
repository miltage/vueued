var figure;
var itemID;
var path = "/vueued/";

$(function(){

	$(document).on("click", "a[data-type]", function(event){
		event.preventDefault();
		figure = $(this).closest("figure");
		itemID = parseInt(figure.attr("data-id"));
		loadItem($(this));
	});

	$(document).on("click", "#next-item", nextItem);

	$(document).on("click", "#prev-item", prevItem);

	$(document).on("keyup", function(event){
		//console.log(event.which);
		if(!figure) return;
		switch(event.which){
			case 39: nextItem(); break;
			case 37: prevItem(); break;
			case 27: closeItem(); break;
		}
	});

	$(document).on("click", "#comments i.collapse", function(event){
		if($(this).hasClass("icon-marquee-minus")){
			$(this).removeClass("icon-marquee-minus");
			$(this).addClass("icon-marquee-plus");
			$(this).parent().siblings(".comment").hide();
		}else{
			$(this).removeClass("icon-marquee-plus");
			$(this).addClass("icon-marquee-minus");			
			$(this).parent().siblings(".comment").show();
		}
	});

	$(document).on("click", "#header span#account", function(event){
		$("#header ul#actions").clearQueue();
		$("#header ul#actions").animate({right: parseInt($("#header ul#actions").css("right"))<-200?"0px":"-250px"}, 800, "easeOutExpo");
	});

	$(document).on("click", "#header ul#nav li", function(event){
		$("#header ul#nav li").removeClass("active");
		$(this).addClass("active");
	});

	$(document).on("click", "span.icon-heart", function(event){
		likeItem($(this));
	});

	$(document).on("click", "span.icon-ribbon", function(event){
		bookmarkItem($(this));
	});

	$(document).on("click", "span.icon-flag", function(event){
		flagItem($(this));
	});

	$(document).on("click", "span.icon-cross", function(event){
		closeItem();
	});

	$(document).on("click", "#overlay", function(event){
		if(event.pageX < $("#content-wrapper").position().left + $("#prev-item").position().left ||
			event.pageX > $("#content-wrapper").position().left + $("#content-wrapper").width() + 100)
			closeItem();
	});

	$(document).on("click", ".admin a.clear", function(event){
		event.preventDefault();
		clearReports();
	});

	$(document).on("click", ".admin a.hide", function(event){
		event.preventDefault();
		hideItem();
	});

	$(document).on("click", ".admin a.show", function(event){
		event.preventDefault();
		showItem();
	});

	$(document).on("click", ".admin a.delete", function(event){
		event.preventDefault();
		deleteItem();
	});

	$(document).on("change", "#sidebar select", function(event){
		activateList($(this).val());
	});

	$(document).on("click", "#sidebar ul.list li", function(event){
		activateList($(this).attr("data-id"));
	});

	$(document).on("mouseover", "#content-buttons span.icon", function(event){
		$("#content-buttons span.title").html($(this).attr("data-title")).show();
	});

	$(document).on("mouseout", "#content-buttons span.icon", function(event){
		$("#content-buttons span.title").hide();
	});

	$(document).on("keypress", ".tagit-new input", function(e){
	    var regex = new RegExp("^[a-zA-Z0-9 ]+$");
	    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	    if (regex.test(str)) {
	        return true;
	    }

	    e.preventDefault();
	    return false;
	});

	$(document).on("click", "a.reply, button.comment", function(event){
		event.preventDefault();
		var parent = $(this).attr("data-parent");
		$(this).parent().after("<span class=\"response\"><textarea></textarea><small></small><button data-parent=\""+parent+"\" class=\"save\">Save</button></span>");
	});

	$(document).on("click", "span.response button.save", function(event){
		var content = $(this).prevAll("textarea").val();
		var small = $(this).prev("small");
		if(!content.length){
			small.html("You forgot to type something!");
			$(this).prevAll("textarea").focus();
			return false;
		}
		$(this).prop("disabled", true);
		small.html("Sending...");
		postComment(content, $(this));
	});

	$(document).on("click", ".comment a.report", function(event){
		event.preventDefault();
		reportComment($(this));
	});

	$(document).on("click", ".comment a.remove", function(event){
		event.preventDefault();
		removeComment($(this));
	});

	$(document).on("click", ".comment a.clear", function(event){
		event.preventDefault();
		clearCommentReports($(this));
	});

	$(document).on("click", ".comment a.delete", function(event){
		event.preventDefault();
		var c = confirm("Are you sure you want to delete this comment?");
		if(c) removeComment($(this));
	});

	$(document).on("click", ".comment a.edit", function(event){
		event.preventDefault();
		var id = $(this).attr("data-id");
		var targetSpan = $(this).parent().prev("span");
		$.get(path+"comment/get/"+id+"/", function(data){
			console.log(data);
			targetSpan.replaceWith("<span class=\"edit\"><textarea>"+data.content+"</textarea><small></small><button data-id=\""+id+"\" class=\"save\">Save</button></span>");
		});
	});

	$(document).on("click", "span.edit button.save", function(event){
		var content = $(this).prevAll("textarea").val();
		var small = $(this).prev("small");
		if(!content.length){
			small.html("You forgot to type something!");
			$(this).prevAll("textarea").focus();
			return false;
		}
		$(this).prop("disabled", true);
		small.html("Saving...");
		editComment(content, $(this));
	});

	initGrid();

	$(window).resize(initGrid);

});

function activateList(list_id){
	$.post(path+"lists/activate/"+list_id+"/", function(data){
		window.location.href = window.location.href; // refresh
	});
}

function loadGrid(type){
	console.log(path+type);
	$("#grid").load(path+type, initGrid);
}

function loadItem(a){
	var type = a.attr("data-type");
	var id = a.attr("data-id");

	$("#overlay-content").load(path+"item/"+id+"/.json", function(){	
		$("#next-item").css("display", figure.next().length?"block":"none");
		$("#prev-item").css("display", figure.prev().length?"block":"none");
		$("#overlay, #overlay-dark").fadeIn();
		$("#overlay-bg").height($("#overlay-content").height());

		$("body").css("overflow-y", "hidden");
	});
	
}

function closeItem(){
	figure = null;
	$("#overlay, #overlay-dark").fadeOut(400, function(){
		$("#overlay #content div").html("");
		$("body").css("overflow-y", "auto");
	});
}

function clearCommentReports(button){
	var id = button.attr("data-id");
	$.post(path+"comment/clear/"+id+"/", function(data){
		button.remove();
	});
}

function removeComment(button){
	var id = button.attr("data-id");
	$.post(path+"comment/remove/"+id+"/", function(data){
		button.parents("li").remove();
	});
}

function reportComment(button){
	var id = button.attr("data-id");
	$.post(path+"comment/report/"+id+"/", function(data){
		if(data.success){
			if(button.html() == "report")
				button.html("un-report");
			else
				button.html("report");
		}
	}, 'json');
}

function postComment(comment, button){
	var parent = button.attr("data-parent");
	$.post(path+"comment/post/"+itemID+"/", {content: comment, parent: parent}, function(data){
		if(data.success){
			var ul = button.parent().next("ul");
			if(!ul.length) ul = button.parent().after("<ul></ul>");
			button.parent().next("ul").prepend(data.result);
			button.parent().remove();
		}else{
			button.prev("small").html("Failed to post comment.");
			button.prop("disabled", false);
		}
	}, 'json');
}

function editComment(source, button){
	var id = button.attr("data-id");
	console.log(id, source);
	$.post(path+"comment/edit/"+id+"/", {source: source}, function(data){
		console.log(data);
		if(data.success){
			button.parent().replaceWith("<span>"+data.content+"</span>");
		}else
			console.log("fail");
	}, 'json');
}

function likeItem(button){
	$.post(path+"item/like/"+itemID+"/", function(data){
		if(data.success)
			if(button.hasClass("active")) 
				button.removeClass("active");
			else
				button.addClass("active");
		else
			$("#content-buttons span.title").html("You'll need to be logged in to do that.").show();
	}, 'json');
}

function bookmarkItem(button){
	$.post(path+"item/bookmark/"+itemID+"/", function(data){
		if(data.success)
			if(button.hasClass("active")) 
				button.removeClass("active");
			else
				button.addClass("active");
		else
			$("#content-buttons span.title").html("You'll need to be logged in to do that.").show();
	}, 'json');
}

function flagItem(button){
	$.post(path+"item/flag/"+itemID+"/", function(data){
		if(data.success)
			if(button.hasClass("active")) 
				button.removeClass("active");
			else
				button.addClass("active");
		else
			$("#content-buttons span.title").html("You'll need to be logged in to do that.").show();
	}, 'json');
}

function clearReports(){
	$.post(path+"item/clear/"+itemID+"/", function(data){
		$("a.clear").remove();
		$("p.numReports").remove();
		$("span.icon-flag").removeClass("active");
	});
}

function hideItem(){
	$.post(path+"item/hide/"+itemID+"/", function(data){
		$("a.hide").html("show").removeClass("hide").addClass("show");
		$("figure[data-id='"+itemID+"']").remove();
		initGrid();
	});
}

function showItem(){
	$.post(path+"item/show/"+itemID+"/", function(data){
		$("a.show").html("hide").removeClass("show").addClass("hide");
	});
}

function deleteItem(){
	$.post(path+"item/delete/"+itemID+"/", function(data){
		$("a.delete").html("item deleted!");
		$("figure[data-id='"+itemID+"']").remove();
		initGrid();
	});
}

function nextItem(){
	if(!figure.next().length) return;
	figure = figure.next();
	loadItem(figure.find("a"));
}

function prevItem(){
	if(!figure.prev().length) return;
	figure = figure.prev();
	loadItem(figure.find("a"));
}

function initGrid(){
	var i=0;
	var numFigures = $("#grid").children("figure").length;
	$("#grid").width("80%");
	var rowLength = Math.floor($("#grid").width()/200);
	$("#grid").width(200*rowLength+"px");
	$("#grid figure").each(function(){
		$(this).removeClass().find("div").removeClass();
		$(this).find("small").removeClass();

		if(i%rowLength == 0 && parseInt(i/rowLength) == Math.ceil(numFigures/rowLength)-1){
			$(this).addClass("bl-corner").find("div").addClass("bl-corner");
			$(this).find("small").addClass("bl-corner");
		}

		if(i == rowLength-1 || i == numFigures-1 && i < rowLength){
			$(this).addClass("tr-corner").find("div").addClass("tr-corner");
		}

		if(parseInt(i/rowLength) == parseInt(numFigures/rowLength)-1 && i%rowLength == rowLength-1 && numFigures%rowLength < rowLength){
			$(this).addClass("br-corner").find("div").addClass("br-corner");
		}

		if(parseInt(i/rowLength) == Math.ceil(numFigures/rowLength)-1 || parseInt(i/rowLength) == Math.ceil(numFigures/rowLength)-2 && i%rowLength >= numFigures%rowLength && numFigures%rowLength){
			$(this).addClass("bordered");
		}

		i++;
	});

	$("#grid figure:first-child, #grid figure:first-child div").addClass("tl-corner");
	$("#grid figure:last-child, #grid figure:last-child div").addClass("br-corner");
}

function getQueryParams(qs){
    qs = qs.split("+").join(" ");

    var params = {}, tokens, re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}