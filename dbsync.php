<?php
	/*
		Function for syncing two databases



	*/

	function db_sync($db1, $db2, $show=false){

		// CONNECT TO FIRST DATABASE
		mysql_connect($db1['db_host'], $db1['db_username'], $db1['db_password']) or throwException('Could not connect. ' . mysql_error());
		$db_name = $db1['db_name'];
		mysql_select_db($db_name) or throwException('Could not select database. ' . mysql_error());

		$result = mysql_query("SHOW TABLES FROM $db_name") or throwException(mysql_error());
		$table_names = array();
		while ($row = mysql_fetch_row($result)){
		    $table_names[] = $row[0];
		}

		$query = "";
		$master_tables = array();

		// ADD NEW TABLES

		foreach($table_names as $table){
			$result = mysql_query("DESCRIBE ".$table) or throwException(mysql_error());
			$query .= "CREATE TABLE IF NOT EXISTS `$table` (\n";
			$primary = null;
			$table_struct = array();
			$table_struct['Name'] = $table;
			while($row = mysql_fetch_array($result)){
				$table_struct[$row['Field']] = $row;
				$query .= "\t`".$row['Field']."` ".$row['Type']." ".($row['Null'] == "NO"?'NOT NULL':'NULL');
				if($row['Extra'] != "") $query .= " ".strtoupper($row['Extra']);
				if($row['Default'] != "") $query .= " DEFAULT ".($row['Default'] == "NULL"?"NULL":"'".$row['Default']."'");
				$query .= ",\n";
				if($row['Key'] != "") $primary = $row;
			}

			$master_tables[$table] = $table_struct;

			if(isset($primary)) $query .= "\tPRIMARY KEY (`".$primary['Field']."`)\n";
			else $query = substr($query, 0, strlen($query)-2)."\n";

			$query .= ");\n\n";
		}

		// CONNECT TO SECOND DATABASE
		mysql_connect($db2['db_host'], $db2['db_username'], $db2['db_password']) or throwException('Could not connect. ' . mysql_error());
		$db_name = $db2['db_name'];
		mysql_select_db($db_name) or throwException('Could not select database. ' . mysql_error());

		$result = mysql_query("SHOW TABLES FROM $db_name") or throwException(mysql_error());
		$table_names = array();
		while ($row = mysql_fetch_row($result)){
		    $table_names[] = $row[0];
		}

		$slave_tables = array();

		foreach($table_names as $table){
			$result = mysql_query("DESCRIBE ".$table) or throwException(mysql_error());
			$table_struct = array();
			$table_struct['Name'] = $table;
			while($row = mysql_fetch_array($result)){
				$table_struct[$row['Field']] = $row;
			}

			$slave_tables[$table] = $table_struct;
		}

		// DELETE TABLES

		foreach($slave_tables as $table_name => $slave_table){
			
			if(!isset($master_tables[$table_name])){
				$query .= "DROP TABLE IF EXISTS $table_name;\n\n";
			}
		}

		// ALTER EXISTING TABLES

		foreach($master_tables as $table_name => $master_table){
			$first = true;
			if(!in_array($table_name, $slave_tables)) continue;
			$slave_table = $slave_tables[$table_name];
			foreach($master_table as $master_field){
				// Only alter is table exists
				if($first || $slave_table == null){
					$first = false;
					continue;
				}

				$field_name = $master_field['Field'];

				// Field does not exist in slave table
				if(empty($slave_table[$field_name]['Field'])){
					$query .= "ALTER TABLE $table_name\n";
					$query .= "ADD `".$field_name."` ".$master_field['Type']." ".($master_field['Null'] == "NO"?'NOT NULL':'NULL');
					if($master_field['Extra'] != "") $query .= " ".strtoupper($master_field['Extra']);
					if($master_field['Default'] != "") $query .= " DEFAULT ".$master_field['Default'];
					if($master_field['Key'] != ""){
						$query .= ", ADD PRIMARY KEY(`".$field_name."`)";
					}

					$query .= ";\n\n";

					
				}
				// Field exists, so compare it
				else {
					$diff = array_compare($master_field, $slave_table[$field_name]);

					// Check for differences and update accordingly
					if(count($diff) > 0){
						$query .= "ALTER TABLE $table_name\n";
						$query .= "CHANGE `".$field_name."` `".$field_name."`";
						$query .= " ".$master_field['Type'];
						$query .= " ".($master_field['Null'] == "NO"?'NOT NULL':'NULL');
						if($master_field['Extra'] != "")$query .= " ".strtoupper($master_field['Extra']);
						if($master_field['Default'] != "") $query .= " DEFAULT ".$master_field['Default'];
						if(isset($diff['Key'])){
							if($master_field['Key'] == "PRI"){
								$query .= ", ADD PRIMARY KEY(`".$field_name."`)";
							}else{
								$query .= ", DROP PRIMARY KEY";
							}
						}
						$query .= ";\n\n";
					}
					
				}

			}

			$first = true;
			foreach($slave_table as $slave_field){
				if($first){
					$first = false;
					continue;
				}

				$field_name = $slave_field['Field'];

				// Field does not exist in master table, it needs to be dropped
				if($master_table[$field_name]['Field'] == ""){
					$query .= "ALTER TABLE $table_name\n";
					$query .= "DROP COLUMN `".$field_name."`;\n\n";
				}

				
			}
		}

		// RUN THE QUERIES

		if($show) echo "<pre>".$query."</pre>";

		file_put_contents("schema.sql", $query);

		exit;

		if($show) echo "Running update on table ".$db2['db_name']."...<br>";

		$queries = preg_split("/;+(?=([^'|^\\\']*['|\\\'][^'|^\\\']*['|\\\'])*[^'|^\\\']*[^'|^\\\']$)/", $query); 
		foreach ($queries as $sql){ 
		   if (strlen(trim($sql)) > 0) mysql_query($sql) or die(mysql_error());; 
		}

		if($show) echo "Done.";
	}

	function throwException($e){
		throw new Exception($e, 1);
	}

	function array_compare($a, $b) {
	    $map = $out = array();
	    foreach($a as $key=>$val) $map[$key] = $val;
	    foreach($b as $key=>$val) 
	    	if($a[$key] == $val)
	    		unset($map[$key]);
	    return $map;
	}

?>