var figure;

$(function(){

	$( '.swipebox' ).swipebox();

	$(".grid-item img").load(function(){
		$(this).parent().parent().addClass("loaded");
		$(this).parent().css("background-image", "url('"+$(this).attr('src')+"')");
		$(this).remove();
	});

	var $grid = $('.grid').isotope({
		// options
		itemSelector: '.grid-item'
	});

	$('#cats li').click(function() {
	  var filterValue = $(this).attr('data-cat') == "*"?".grid-item":"[data-cat*='"+$(this).attr('data-cat')+"']";
	  $grid.isotope({ filter:  filterValue });
	});

});